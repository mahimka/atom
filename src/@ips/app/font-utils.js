import Sizer from '@ips/app/sizer'
import { createStyle } from '@ips/app/css-utils'
import * as __ from '@ips/app/hidash'

import uniqueNumber from '@ips/app/unique-number'
const stylesheet = createStyle('text-sizes')
const textStyles = {}
// const name2style = {}

trace('textStyles', textStyles)

const genDegradingMediaRule = (className, fontSize, fontSizeMobile, exp, ssizes, mobileSmall, w)=>(__.map(ssizes, (s, i)=>
`@media (max-width: ${s+w}px) {
    .${ className } {
        font-size: ${((i==ssizes.length-1)&&fontSizeMobile) ? fontSizeMobile: Math.floor(fontSize/Math.pow(exp, (i+1)))}px;
    }
}`).join('\n')) + 
`
@media (max-width: ${mobileSmall}px) {
    .${ className } {
        font-size: calc(var(--rvw, 1vw) * ${mobileSmall});
    }
}`

const genDegradingNameRule = (className, fontSize, fontSizeMobile, exp, snames, mobileSmall, w)=>(__.map(snames, (s, i)=>
`
.${s} .${ className } {
        font-size: ${((i==snames.length-1)&&fontSizeMobile) ? fontSizeMobile: Math.floor(fontSize/Math.pow(exp, (i+1)))}px;
}`).join('\n')) + 
`
@media (max-width: ${mobileSmall}px) {
    .${ className } {
        font-size: calc(var(--rvw, 1vw) * ${mobileSmall});
    }
}`

export const createTextStyle = ({ name, fontFamily, fontFamilyDefault, fontSize, fontSizeMobile, fontWeight, fontStyle, lineHeight, letterSpacing, color, opacity, textTransform, textAlign, margin, degradeExp = 1, fontSizes=[] }, uid)=>{
    if(isNaN(+fontSize)){
        error('cannot create text style with fontSize', fontSize)
        return
    }
    // const styleSign = `ts_${name||''}_${fontFamily||''}_${fontSize}_${degradeExp?('deg'+degradeExp):''}_${fontSizes.join('_')}`.replace(/\./g, '')
    const styleSign = `ts_${uid||''}`.replace(/\./g, '')

    if(textStyles[uid]){
        // trace('FontUtils.createTextStyle reusing', styleSign)
        textStyles[uid][4]++ // increment useCount
        return textStyles[uid]
    }

    const className = styleSign
    // trace('FontUtils.createTextStyle', className)

    const baseRule = stylesheet.addRule('.'+className, 
        `
        font-size:${fontSize}px;
        ${lineHeight?'line-height:'+lineHeight+';':''}
        ${letterSpacing?'letter-spacing:'+letterSpacing+';':''}
        ${fontFamily?'font-family:'+fontFamily+(fontFamilyDefault?(' ,'+fontFamilyDefault):'')+';':''}
        ${fontWeight?'font-weight:'+fontWeight+';':''}
        ${fontStyle?'font-style:'+fontStyle+';':''}
        ${color?'color:'+color+';':''}
        ${opacity?'opacity:'+opacity+';':''}
        ${textTransform?'text-transform:'+textTransform+';':''}
        ${textAlign?'text-align:'+textAlign+';':''}
        ${margin?'margin:'+margin+';':''}
        `)

    // const ssizes = [...Sizer.sizes()].reverse() // cant do reverse on the sizer arr since it's inplace. need to clone
    // const mediaRules = stylesheet.addRaw(genDegradingMediaRule(className, fontSize, fontSizeMobile, degradeExp, ssizes, 350, Modernizr['platform-windows'] ? 17: 0))

    const snames = [...Sizer.names()].reverse()
    snames.shift() // cant do reverse on the sizer arr since it's inplace. need to clone

    const rule = genDegradingNameRule(className, fontSize, fontSizeMobile, degradeExp, snames, 350, Modernizr['platform-windows'] ? 17: 0)
    // trace('fontSizeMobile', fontSizeMobile, rule)
    const mediaRules = stylesheet.addRaw(rule)


    textStyles[uid] = [className, uid, baseRule, mediaRules, 1] // useCount = 1
    // name2style[name] = textStyles[styleSign]

    return textStyles[uid]
}

export const removeTextStyle = style=>{
    // const style = textStyles[sign]
    if(!style)
        return
    
    // trace('FontUtils.removeTextStyle', style)

    style[4]-- // decrement useCount
    if(style[4]) // still in use
        return

    const [className, uid, baseRule, mediaRules] = style
    stylesheet.removeRaw(mediaRules)
    stylesheet.removeRule(baseRule)
    textStyles[uid] = null
    // name2style[name] = null
}

// export const findTextStyle = name=>name2style[name]
