
if(console && console.log){
    window.info = function(){ console.log.apply(console, arguments) };
    window.trace = function(){ console.log.apply(console, arguments) };
    window.warn = function(){ console.warn.apply(console, arguments) };
    window.error = function(){ console.error.apply(console, arguments) };
}else{
    window.info =
    window.trace =
    window.warn =
    window.error = function(){};

    if (!window.console) window.console = {};
    if (!window.console.log) window.console.log = function () { };
}
