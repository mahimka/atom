import { configure, setAddon } from '@storybook/react';
import JSXAddon from 'storybook-addon-jsx/lib/index';
setAddon(JSXAddon);

// automatically import all files ending in *.stories.js
configure(require.context('../stories', true, /\.stories\.js$/), module);
