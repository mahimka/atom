import React from 'react';

import { storiesOf } from '@storybook/react';
import Base from '@ips/react/components/base';

import '@ips/app/trace'
import '@ips/app/normalize.css.styl'
import '@ips/app/modernizr'
import '@ips/app/sizer'

//storiesOf('Base', module).addWithJSX = storiesOf('Base', module).add;

storiesOf('Components/Base', module)
  .add('Doc', () => <div>mebase</div> )
