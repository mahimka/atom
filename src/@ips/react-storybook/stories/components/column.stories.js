import React from 'react';

import { storiesOf } from '@storybook/react';
// import { withInfo } from '@storybook/addon-info';


import Slice from '@ips/react/components/slice';
import Column from '@ips/react/components/column';

//import './column.styl'

import { lorem, lorem2 } from '../lorem.js'


const infoStyle = {
  //inline: true,
	text: `
          description or documentation about my component, supports markdown

          ~~~js
          <Slice> Content </Slice>
          ~~~
        `,  
  styles: stylesheet => ({
    // Setting the style with a function
    ...stylesheet,
    header: {
      ...stylesheet.header,
      h1: {
        ...stylesheet.header.h1,
        color: 'green', // Still inlined but with green headers!
      },
    },
  }),
}

const info = `Here is some general info about Column component
	  	Column
	  		Container
	  		SliceBox
	  		SliceInner
	  		<br>
	  		<br>
	  		props:
	  		style
	  		className
	  		height
	  		width
	  		align`


const addFn = 'addWithJSX'

storiesOf('Components/Column', module)
  .add('Doc', ()=><div dangerouslySetInnerHTML={{ __html:info }}/>)
  // .addDecorator(withInfo)
  
  [addFn]('(default)', () => (
    <Slice className="show" ><Column>{ lorem }</Column></Slice>
  ), {
    info:infoStyle,
  })

  [addFn]('Column 8', () => (
    <Slice className="show" ><Column width="8">{ lorem }</Column></Slice>
  ))

  [addFn]('Column 4/4', () => (
    <Slice className="show" ><Column width="4" left="4">{ lorem }</Column></Slice>
  ))


  [addFn]('Column composition', () => ([
    <Slice className="show" >
      <Column width="5">{ lorem }</Column>
      <Column width="7">{ lorem }</Column>
      <Column width="6">{ lorem }</Column>
      <Column width="4" left="2">{ lorem }</Column>
    </Slice>
  ]))

