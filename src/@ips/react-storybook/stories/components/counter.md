# Counter

Счётчик - динамический, форматированный счётчик

``` <Counter current="500" target="1000"/>```

## Community

Acorn is open source software released under an
[MIT license](https://github.com/acornjs/acorn/blob/master/LICENSE).


**parse**`(input, options)` is used to parse a JavaScript program.
The `input` parameter is a string, `options` can be undefined or an
object setting some of the options listed below. The return value will
be an abstract syntax tree object as specified by the
[ESTree spec][estree].

- **sourceType**: Indicate the mode the code should be parsed in. Can be
  either `"script"` or `"module"`. This influences global strict mode
  and parsing of `import` and `export` declarations.

- **onInsertedSemicolon**: If given a callback, that callback will be
  called whenever a missing semicolon is inserted by the parser. The
  callback will be given the character offset of the point where the
  semicolon is inserted as argument, and if `locations` is on, also a
  `{line, column}` object representing this position.

- **onTrailingComma**: Like `onInsertedSemicolon`, but for trailing
  commas.

