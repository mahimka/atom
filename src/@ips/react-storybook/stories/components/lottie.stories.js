import React from 'react';

import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withReadme, withDocs, doc } from 'storybook-readme';


import Readme from './lottie.md';

import Lottie from '@ips/react/components/lottie';

// import './slice.styl'

storiesOf('Components/Lottie', module)
  .add('Doc', doc(Readme))

storiesOf('Components/Lottie', module)
  .addWithJSX('(default)', () => (
    <Lottie anim="lottie1"/>
  ))
