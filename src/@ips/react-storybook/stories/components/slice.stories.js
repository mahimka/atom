import React from 'react';

import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withReadme, withDocs, doc } from 'storybook-readme';


import Readme from './slice.md';

import Slice from '@ips/react/components/slice';
import SliceBox from '@ips/react/components/slice-box';
import SliceInner from '@ips/react/components/slice-inner';

import './slice.styl'

import { lorem2 } from '../lorem.js'

const text = lorem2

const infoStyle = {
  //inline: true,
	text: `
          description or documentation about my component, supports markdown

          ~~~js
          <Slice> Content </Slice>
          ~~~
        `,  
  styles: stylesheet => ({
    // Setting the style with a function
    ...stylesheet,
    header: {
      ...stylesheet.header,
      h1: {
        ...stylesheet.header.h1,
        color: 'green', // Still inlined but with green headers!
      },
    },
  }),
}

const info = `Here is some general info about Slice component
	  	Slice
	  		Container
	  		SliceBox
	  		SliceInner
	  		<br>
	  		<br>
	  		props:
	  		style
	  		className
	  		height
	  		width
	  		align`

const addFn = 'addWithJSX'


storiesOf('Components/Slice', module)
  .add('Info', doc(Readme))
  //.add('Info', withReadme(Readme, ()=><div dangerouslySetInnerHTML={{ __html:info }}/>))
  .addDecorator(withInfo)

storiesOf('Components/Slice/General', module)
  [addFn]('Slice center/12 (default)', () => (
    <Slice className="show" >{ text }</Slice>
  ), {
    info:infoStyle,
  })

  [addFn]('Slice right align', () => (
    <Slice className="show" align='end' >{ text }</Slice>
  ))

  [addFn]('Slice left align', () => (
    <Slice className="show" align='start' >{ text }</Slice>
  ))

  [addFn]('Slice width 8, center', () => (
    <Slice className="show" width='8' align='center' >{ text }</Slice>
  ))

storiesOf('Components/Slice/Advanced', module)
  [addFn]('SliceBoxes', () => (
    [ 
    	<SliceBox style={{ backgroundColor:'green', height:'300px' }} ></SliceBox>,
    	<SliceBox  style={{ backgroundColor:'red', height:'300px' }} ></SliceBox>,
    ]
  ))

  [addFn]('SliceBox+SliceInner', () => (
    <SliceBox className="show2">
    	Something on the back (in a SliceBox)
    	<SliceInner width='8' align='center' >{ text }</SliceInner>
    </SliceBox>
  ))

storiesOf('Components/Slice/Solutions', module)
  [addFn]('mix', () => (
  [
    <Slice className="show" align='end' >{ text }</Slice>,
    <Slice style={{ backgroundColor:'red' }} className="show" align='start' >{ text }</Slice>,
    <SliceBox style={{ backgroundColor:'green', height:'300px' }}  className="show" ></SliceBox>,
    <Slice className="show" width='8' align='center' >{ text }</Slice>,
   	<SliceBox style={{ backgroundColor:'red', height:'300px' }} ></SliceBox>,]
  ))
