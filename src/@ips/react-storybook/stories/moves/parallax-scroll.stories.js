import React from 'react';

import { storiesOf } from '@storybook/react';
//import { withReadme, withDocs, doc } from 'storybook-readme';
//import Readme from './image-sequence.md';

import Parallax from   '@ips/react/components/parallax';
import ParallaxScene from   '@ips/react/components/parallax-scene';
import ParallaxLayer from   '@ips/react/components/parallax-layer';

const addFn = 'addWithJSX'

storiesOf('Moves', module)
  .addWithJSX('Parallaxing cover on scroll', () => (
    <Parallax>
      <div>Some stuff</div>
      <ParallaxScene>
        <ParallaxLayer depth="0"><img src="img/para0.png"/></ParallaxLayer>
        <ParallaxLayer depth="60"><img src="img/para1.png"/></ParallaxLayer>
        <ParallaxLayer depth="100"><img src="img/para2.png"/></ParallaxLayer>        
      </ParallaxScene>
      <div style={{ height:"100vh" }}/>
      <div>Some other stuff</div>
      <div style={{ height:"300vh" }}/>
    </Parallax>
  ))
