import React, { Component } from 'react';
import _ from 'lodash'

import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withReadme, withDocs, doc } from 'storybook-readme';


import Timeline from '@ips/react/components/timeline';
import Sticky from  '@ips/react/components/sticky';
import Way from   '@ips/react/components/way';
import Waypoint from   '@ips/react/components/waypoint';
import Fig from   '@ips/react/components/fig';
import Slice from   '@ips/react/components/slice';
import Column from   '@ips/react/components/column';

import ImageSequence from '@ips/react/components/image-sequence';
import Fixed from  '@ips/react/components/fixed';
import DText from  '@ips/react/components/dtext';

import Jogwheel from '@ips/react/components/jogwheel'
import ScrollTrackerC from '@ips/react/components/scroll-tracker'
const ScrollTracker = ScrollTrackerC.ScrollTrackerF

import Overlay from '@ips/react/components/overlay'

import { lorem, loremLong } from '../lorem'
import threesixty from '../threesixty'

import './measure.styl'

import './sticky-scroll-switch.stories.styl'

class Measure extends Component{
  state = { count: 0 }

  componentWillMount(){
    const count = 50;
    const dartoom = _.times(count, c => <div key={ c } style={{ top: (c*100) + 'px' }}>{ c*100 }</div>)
    trace('moundartoom', dartoom)
    this.setState({ count, dartoom })
  }

  render(){
    trace('rendartoom', this)
    const { dartoom } = this.state
    return <div className="measure">{ dartoom }</div>
  }
}

storiesOf('Moves', module)
  .addWithJSX('Sticky + Switch by scroll', () => (
  	<div>
      <Measure/>
  		<Way name="way" decorate/>
      <Slice>
        <h1>Scroll down and watch</h1>
      </Slice>
      <Slice>
        <Column width="7">{ loremLong }</Column>
        <Column width="4" left="1">{ lorem }</Column>
      </Slice>
  		<div>
  			<Sticky>
          <DText text="expr event('way', 'point', 'index')"/>
  				<Timeline current="expr event('way', 'point', 'index')" mode="fade">
  					<Fig src="threesixty_1.jpg"/>
  					<Fig src="threesixty_64.jpg"/>
  					<Fig src="threesixty_144.jpg"/>
  				</Timeline>
  			</Sticky>
        <Waypoint way="way" edge="-100vh"/>
  			<div style={{ height:"50vh" }}/>
  			<Waypoint way="way" edge="-30vh"/>
  			<div style={{ height:"30vh" }}/>
  			<Waypoint way="way"/>
  			<div style={{ height:"30vh" }}/>
        <Waypoint way="way" edge="10vh"/>
  		</div>
      <Slice>
        <Column width="7">{ loremLong }</Column>
        <Column width="4" left="1">{ lorem }</Column>
      </Slice>
  	</div>
  ))
//          <ImageSequence images={ threesixty } current="expr tween()*70"/>
//         <ImageSequence images={ threesixty } current="expr tween(current, (event('way', 'point', 'index')+1)*40, 1, 'Cubic.Out', [0])"/>


storiesOf('Moves', module)
  .addWithJSX('Tween a sequence by scroll', () => (
    <div>
      <Fixed cover>
        <ImageSequence images={ threesixty } current="expr tween(current, at([0, 100, 30, 60], event('way', 'point', 'index')+1), 1, 'Cubic.Out', [0])"/>
      </Fixed>
      <Way name="way" decorate/> 
      <Slice>
        <h1>Scroll down and watch</h1>
      </Slice>
      <Slice>
      </Slice>
      <div>
        <Waypoint way="way" edge="10vh"/>
        <div style={{ height:"50vh" }}/>
        <Waypoint way="way"/>
        <div style={{ height:"30vh" }}/>
        <Waypoint way="way"/>
        <div style={{ height:"100vh" }}/>
      </div>
      <Slice>
        <Column width="7">{ loremLong }</Column>
        <Column width="4" left="1">{ lorem }</Column>
      </Slice>
    </div>
  ))

storiesOf('Moves', module)
  .addWithJSX('Stiky+ScrollTracker+Jogwheel for slow panning', () => (
    <div>
      <Slice>
        <h1>Scroll down and watch</h1>
        <Column width="4" right="6">
          { lorem }
          { lorem }
        </Column>

      </Slice>
      <Slice>
        <Column width="6">
          <ScrollTracker name="st1" throttle="0">
            { loremLong }
            { loremLong }
            { loremLong }
          </ScrollTracker>
        </Column>
        <Column width="6">
            <Sticky>
              <Jogwheel className="slopan" progress="expr event('st1', 'update', 'pos')">
                <div>LENGTHY THING</div>
              </Jogwheel>
            </Sticky>
        </Column>
      </Slice>
      <div>
        <Waypoint way="way" edge="10vh"/>
        <div style={{ height:"50vh" }}/>
        <Waypoint way="way"/>
        <div style={{ height:"30vh" }}/>
        <Waypoint way="way"/>
        <div style={{ height:"100vh" }}/>
      </div>
      <Slice>
        <Column width="7">{ loremLong }</Column>
        <Column width="4" left="1">{ lorem }</Column>
      </Slice>
    </div>
  ))

storiesOf('Moves', module)
  .addWithJSX('Stiky+ScrollTracker+Jogwheel for whaling', () => (
    <div>
      <Slice>
        <h1>Scroll down and watch</h1>
        <Column width="4" right="6">
          { lorem }
          { lorem }
        </Column>

      </Slice>
      <Slice>
        <Overlay cover>
            <Sticky>
              <Jogwheel className="whale" progress="expr tween(progress, event('st2', 'update', 'pos'), 1, 'Cubic.Out', [0]) 0">
                <div>>=IMAWHALE></div>
              </Jogwheel>
            </Sticky>
        </Overlay>
        <Column left="3" width="6">
          <ScrollTracker name="st2" throttle="0">
            { loremLong }
            { loremLong }
            { loremLong }
          </ScrollTracker>
        </Column>
        <Column width="6">
        </Column>
      </Slice>
      <div>
        <Waypoint way="way" edge="10vh"/>
        <div style={{ height:"50vh" }}/>
        <Waypoint way="way"/>
        <div style={{ height:"30vh" }}/>
        <Waypoint way="way"/>
        <div style={{ height:"100vh" }}/>
      </div>
      <Slice>
        <Column width="7">{ loremLong }</Column>
        <Column width="4" left="1">{ lorem }</Column>
      </Slice>
    </div>
  ))
