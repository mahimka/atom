import React, { Fragment } from 'react';

import { storiesOf } from '@storybook/react';
//import { withReadme, withDocs, doc } from 'storybook-readme';
//import Readme from './image-sequence.md';

import { Slice, Column } from '@ips/react/components/layout'
import Sticky from '@ips/react/components/sticky'
import Text from '@ips/react/components/text'
import Overlay from '@ips/react/components/overlay'
import Pic from '@ips/react/components/pic'
import { lorem, lorem2, loremLong } from '../lorem'

import './pobeda-bluewhite.stories.styl'

const paraLorem = `<p>${lorem}</p>`
const lorem3= lorem2+ lorem

storiesOf('Moves/Sticky Layers', module)
  .addWithJSX('Pobeda blue-white transition', () => (
    <Slice width="6">
        <Column className="backwhite">
            <Text mod="blue" text={loremLong}/>
        </Column>
        <Column minHeight="200vh">
            <Overlay cover className="blue-white"/>
            <Sticky>
                <Text mod="white big" text="POBEDA"/>
                <Text mod="blue" text={lorem3}/>
            </Sticky>
        </Column>
        <Column className="backwhite" minHeight="100vh">
            <Text mod="blue" text={loremLong}/>
        </Column>
        <Column minHeight="400vh" width="100%">
            <Sticky top="0">
                <Pic src="bars-base.svg"/>
            </Sticky>
            <Sticky top="100vh">
                <Pic src="bars-vals.svg"/>
            </Sticky>
        </Column>        

        <Column className="backwhite" minHeight="100vh">
            <Text mod="blue" text={loremLong}/>
        </Column>
    </Slice>
  ))


storiesOf('Moves/Sticky Layers', module)
  .addWithJSX('Halftone layers', () => (
    <Slice width="10">
        <Column className="backwhite">
            <Text mod="blue" text={loremLong}/>
        </Column>
        <Column minHeight="400vh" className="halftones">
            <Sticky top="0">
                <Pic src="halftone2-b.png"/>
            </Sticky>
            <Sticky top="100vh">
                <Pic src="halftone2-m.png"/>
            </Sticky>
            <Sticky top="200vh">
                <Pic src="halftone2-c.png"/>
            </Sticky>
            <Sticky top="300vh">
                <Pic src="halftone2-y.png"/>
            </Sticky>
        </Column>
        <Column className="backwhite" minHeight="100vh">
            <Text mod="blue" text={loremLong}/>
        </Column>
    </Slice>
  ))

storiesOf('Moves/Sticky Layers', module)
  .addWithJSX('Text Columns', () => (
    <Fragment>
        <Slice width="6">
            <Column className="backwhite">
                <Text mod="blue" text={loremLong}/>
            </Column>
        </Slice>    
        <Column width="100%" minHeight="300vh">
            <Sticky>
                <Slice width="6">
                    <Column width="2" left="0" minHeight="80vh">
                        <Text mod="blue" text={lorem}/>
                    </Column>
                </Slice>
            </Sticky>
            <Sticky>
                <Slice width="6">
                    <Column width="2" left="2" minHeight="80vh">
                        <Text mod="blue" text={lorem}/>
                    </Column>
                </Slice>
            </Sticky>
            <Sticky>
                <Slice width="6">
                    <Column width="2" left="4" minHeight="80vh">
                        <Text mod="blue" text={lorem}/>
                    </Column>
                </Slice>
            </Sticky>
        </Column>
        <Slice width="6">
            <Column className="backwhite">
                <Text mod="blue" text={loremLong}/>
            </Column>
        </Slice>          
    </Fragment>
  ))

