import './gooey-spinner.styl'

const tpl= `<div class="gooey-spinner">
        <div class="centered">
            <div class="blob-1"></div>
            <div class="blob-2"></div>
        </div>
        <div class="somcha"/>
    </div>)`

export default (container, opts)=>{
    const $el = document.createElement('div')
    $el.innerHTML = tpl
    container.appendChild($el)
    return {
        $el,
        loaded(){
            container.removeChild($el)
        }
    }
}
