import React, { Component, useRef, useState, useEffect, useContext } from 'react'
import { useMemo, useCallback } from 'use-memo-one'

import viewportUnitsBuggyfill from 'viewport-units-buggyfill'
viewportUnitsBuggyfill.init();

import './main.styl'
import * as __ from '@ips/app/hidash'

import Preloader from '@ips/app/media-preloader'
import { PreloadContext, usePreloader } from '@ips/react/components/preload'

const Main = p=>{

    const ref = useRef()
    const preloader = usePreloader(Preloader)

    const { mode, project } = p.mountData || {}

    trace('main.render', project, mode)

    return  <div ref={ref} className={ `app ${project||''} mode-${ mode }` }>
                <PreloadContext.Provider value={preloader.context()}>
                    { project }
                </PreloadContext.Provider>
           </div>
}

export default Main

    