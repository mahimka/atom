import React, { Component } from 'react'
import { ActivationContext } from './activation'

//require('intersection-observer');
import * as Google from '@ips/app/google-tag-manager'
import * as Metrika from '@ips/app/metrika'

export default class AnalyticsAnchor extends Component{

    state = { visible: false }

    render(){
        return <div ref={ ref=> this.$el = ref } data-id={ this.props.id }/>
    }

    componentDidMount(){
        let options = {
            root: null,
            // rootMargin: "0px",
            // threshold: .5//buildThresholdList()
        };

        const handleIntersect = (inters)=>{
            if(!this.context.active)
                return

            // console.log('inters', this, self, inters)
            if(!this.state.visible && inters[0].intersectionRatio > 0){
                this.setState({ visible: true })
                Google.event('anchor', { anchor:this.props.id })
                Metrika.event('anchor', { anchor:this.props.id })
            }
        }

        const observer = new IntersectionObserver(handleIntersect, options);
        observer.observe(this.$el);
    }
}

AnalyticsAnchor.contextType = ActivationContext

//     props:['id'],
//     data(){
//         return { 
//             visible:false,
//         }
//     },
