import React, { Component, useRef, useState, useEffect, useLayoutEffect, useContext, forwardRef, createContext } from 'react'
import { useMemo, useCallback } from 'use-memo-one'

import './layout.styl'

import { useSizerSize } from '@ips/react/components/utils/use-sizer'
import { createStyle, useStyle } from '@ips/react/components/utils/use-style'
import * as __ from '@ips/app/hidash'
import cx from '@ips/app/classnamex'

import uniqueNumber from '@ips/app/unique-number'

const calcPaddingStyle = p=>{
    if(!p) return ''
    return `padding: ${p};`//.split(' ').map(v=>(v||0)+'px').join(' ')};`
}

const paddingStylesheet = createStyle('padding')

export const usePaddingStyle = p=>{
    const pstyle = useMemo(()=>calcPaddingStyle(p), [p])
    // trace('pstyle', pstyle)
    return useStyle(paddingStylesheet, pstyle)
}

export const createGridStyleContext = (p = {})=>{
    const { props, parent, onUpdate = __.nop } = p
    trace('createGridStyleContext', props)

    const gridStyleSheet = createStyle('grid')

    let _useCount = 1
    const _props = props||{
        gutter:16,
        maxWidth:1440,
    }

    const gutterClass = 'g' + uniqueNumber();
    const gutterCSS = ()=>`padding-left:${_props.gutter}px;padding-right:${_props.gutter}px;`
    const gutterRule = gridStyleSheet.addRule('.'+gutterClass, gutterCSS())


    const setGutter = (g=16)=>{
        _props.gutter = g
        gridStyleSheet.modifyRule(gutterRule, gutterCSS())
    }

    const setMaxWidth = (w=1440)=>{
        _props.maxWidth = w
    }

    return {
        gutterClass:()=>gutterClass,
        gutter:()=>_props.gutter,
        discreteAdaptive:()=>false,
        maxGridWidth:()=>_props.maxWidth,
        maxGrid:()=>'large',
        update:p=>{
            trace('GridStyleContext.update', p)
            setGutter(p.gutter)
            setMaxWidth(p.maxWidth)
            onUpdate(_props)
        },
        acquire:()=>{
            _useCount++
        },
        release:()=>{
            _useCount--
            if(_useCount <= 0)
                gridStyleSheet.destroy()
        },
        _props,
    }
}

export const GridStyleContext = createContext(createGridStyleContext())
export const useGridStyle = ()=>useContext(GridStyleContext)

export const GridStyle = forwardRef((p, ref)=>{
    const gridStyle = useGridStyle()
    // trace('got gridStyle', gridStyle)
    const [ssize] = useSizerSize()
    const mobile = ssize===0
    trace('GridStyle', mobile, ssize)

    const gs = useMemo(()=>mobile?{
        gutter:p.gutterMobile,
        maxWidth:p.maxWidthMobile
    }:p, [mobile])
    useMemo(()=>gridStyle.update(gs), [gs])
    // useMemo(()=>gridStyle.update(p), Object.values(p))
})
GridStyle.displayName = 'GridStyle'

export const GridStyleWrapper = forwardRef((p, ref)=>{
    const parent = useGridStyle()
    const updateCtx = useCallback(p=>{
        // trace('updateCtx', ctx)
        // // if(ctx)
        // //     ctx.destroy()
        // const updatedCtx = {...ctx}//createGridStyleContext({ props:p, parent, onUpdate:updateCtx })
        // trace('updatedCtx', updatedCtx)
        setCtx(ctx)
    })

    const myCtx = useMemo(()=>createGridStyleContext({ parent, onUpdate:updateCtx }),[])

    const [ctx, setCtx] = useState(myCtx)

    // end of life
    useEffect(()=>{
        return ()=>ctx.release()
    },[])

    return (<GridStyleContext.Provider value={ctx}>{p.children}</GridStyleContext.Provider>)
})
GridStyleWrapper.displayName = 'GridStyleWrapper'

const directionClass = (c, r, d)=>c?('dir-column'):(r?'dir-row':'dir-'+d)
const mobDirectionClass = (c)=>c?`mobdir-${c}`:''
const discreteClass = v=>v?'adapt-discrete':''
const classyVal = v=>v==='screen'||!isNaN(+v)
const minHeightClass = v=>(v&&classyVal(v))?('min-height-'+v):''
const widthClass = v=>(v&&classyVal(v))?('width-'+v):''
const leftClass = v=>(v&&classyVal(v))?('left-'+v):''
const rightClass = v=>(v&&classyVal(v))?('right-'+v):''
const alignClass = v=>v?('align-'+v):''
const valignClass = v=>v?('valign-'+v):''
const alignSelfClass = v=>v?('align-self-'+v):''
const overlayClass = v=>v?'pos-overlay':''
const coverClass = v=>v?'size-cover':''
const reverseClass = v=>v?'dir-reverse':''
const wrapClass = v=>v?'dir-wrap':''

const sliceBoxClass = p=>
    `slice ${p.className||''} \
    ${overlayClass(p.overlay)} \
    ${coverClass(p.cover)}\
    ${minHeightClass(p.minHeight)}`

const sliceConClass = p=>
    `slice__container ${p.className||''} \
    ${directionClass(p.column, p.row, 'row')} \
    ${discreteClass(p.discrete)} \
    ${minHeightClass(p.minHeight)} \
    ${widthClass(p.width)} \
    ${leftClass(p.left)} \
    ${rightClass(p.right)} \
    ${alignClass(p.align)} \
    ${alignSelfClass(p.alignSelf)} \
    ${valignClass(p.valign)} \
    ${reverseClass(p.reverse)}
    ${wrapClass(p.wrap)}`

const columnClass = p=>
    `column ${p.className||''} \
    ${directionClass(p.column, p.row, 'column')} \
    ${mobDirectionClass(p.mobDir)} \
    ${minHeightClass(p.minHeight)} \
    ${widthClass(p.width)} \
    ${leftClass(p.left)} \
    ${rightClass(p.right)} \
    ${alignClass(p.align)} \
    ${valignClass(p.valign)} \
    ${reverseClass(p.reverse)}
    ${wrapClass(p.wrap)}`
    
const parseHeight = height=>height == 'screen' ? '100vh' : height
const parseWidth = v=>v == 'screen' ? '100vh' : v

const leftStyle = v=>(v&&!classyVal(v))?`margin-left:${v};`:''
const rightStyle = v=>(v&&!classyVal(v))?`margin-right:${v};`:''
const widthStyle = v=>(v&&!classyVal(v))?`width:${v};`:''
const heightStyle = v=>(v)?`height:${parseHeight(v)};`:''
const minHeightStyle = v=>(v&&!classyVal(v))?`min-height:${parseHeight(v)};`:''
const maxWidthStyle = v=>(v)?`max-width:${parseWidth(v)};`:''

const sliceBoxStyle = p=>`${widthStyle(p.width)}${minHeightStyle(p.minHeight)}${heightStyle(p.height)}`
const sliceConStyle = p=>`${leftStyle(p.left)}${rightStyle(p.right)}${widthStyle(p.width)}${minHeightStyle(p.minHeight)}${heightStyle(p.height)}`
const columnStyle = p=>`${leftStyle(p.left)}${rightStyle(p.right)}${widthStyle(p.width)}${minHeightStyle(p.minHeight)}${heightStyle(p.height)}${maxWidthStyle(p.maxWidth)}`

export const layoutStyleCustom = createStyle('layout')
// trace('layoutStyleCustom', layoutStyleCustom)

export const Slice = forwardRef((p, ref)=>{
    const {className, overlay, cover, onClick, ...other} = p

    return <SliceBox ref={ref} className={className} onClick={onClick} overlay={overlay} cover={cover}>
                <SliceCon {...other}></SliceCon>
            </SliceBox>
})
Slice.displayName = 'Slice'

export const SliceBox = forwardRef((p, ref)=>{
    const customStyle = useMemo(()=>sliceBoxStyle(p), Object.values(p))
    const [uClassName] = useStyle(layoutStyleCustom, customStyle)

    const inref = useRef()

    useEffect(()=>{
        if(!ref) return
        ref.current = inref.current
    })

    useLayoutEffect(()=>{
        if(!p.overlay) 
            return
        if(!inref.current) 
            return
        const cs = getComputedStyle(inref.current.parentElement)
        if(cs.position == 'static')
            inref.current.parentElement.style.position = 'relative'
    }, [p.overlay])

    return (<div ref={inref} className={`${sliceBoxClass(p)} ${uClassName}`} onClick={p.onClick}>
                {p.children}
            </div>)
})
SliceBox.displayName = 'SliceBox'

export const SliceCon = forwardRef((p, ref)=>{
    const customStyle = useMemo(()=>sliceConStyle(p), Object.values(p))
    const [uClassName] = useStyle(layoutStyleCustom, customStyle)
    // trace('SliceCon', uClassName)
    const gridStyle = useGridStyle()
    const [padClassName] = usePaddingStyle(p.padding)

    return (<div ref={ref} className={cx(sliceConClass(p), uClassName, padClassName, 'maxw-'+gridStyle.maxGridWidth())} onClick={p.onClick}>
                {p.children}
            </div>)
})
SliceCon.displayName = 'SliceCon'

export const Column = forwardRef((p, ref)=>{
    const customStyle = useMemo(()=>columnStyle(p), Object.values(p))
    const [uClassName] = useStyle(layoutStyleCustom, customStyle)
    const [padClassName] = usePaddingStyle(p.padding)
    const gridStyle = useGridStyle()
    const gutterClass=p.useGutter?gridStyle.gutterClass():null

    return (<div 
                ref={ref} 
                className={cx(columnClass(p), uClassName, padClassName, gutterClass)} 
                style={p.style}
                onClick={p.onClick}
                onMouseEnter={p.onMouseEnter}
                onMouseLeave={p.onMouseLeave}
                >
                {p.children}
            </div>)
})
Column.displayName = 'Column'

export const Row = forwardRef((p, ref)=><Column ref={ref} row {...p}/>)
Row.displayName = 'Row'
