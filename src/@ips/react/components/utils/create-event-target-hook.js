import { useState, useLayoutEffect } from 'react'
import { useCallback } from 'use-memo-one'

const createEventTargetHook = Target => {
  // const [target, setTarget] = useState(Target)
  // if (typeof Target.addEventListener !== 'function') {
  //   // Runtime check instanceof EventTarget & still checkable in test code
  //   throw new Error('Not an event target')
  // }

  // const fx = useCallback((evt, cb) => {
  //   // trace('noorch', evt)
  //   if(!Target)
  //     return
  //   Target.addEventListener(evt, cb)
  //   return () => {
  //     // trace('denoorch', evt)
  //     Target.removeEventListener(evt, cb)
  //   }
  // }, [Target])

  const useEventHook = (evt, cb, selector) => {
    // trace('useEventHook', evt)
    // useLayoutEffect(()=>{ 
    //   // trace('vzdro')
    //   return fx(evt, cb) } 
    // , [fx, evt, cb])

    useLayoutEffect(()=>{
      // trace('noorch', evt, Target, selector)
      if(!Target)
        return
      const T = selector?T.querySelector(selector):Target
      if(!T)
        return
      // trace('gotenda', T)
      T.addEventListener(evt, cb)
      return () => {
        // trace('denoorch', evt)
        T.removeEventListener(evt, cb)
      }
    }
    , [Target, evt, cb, selector])
        
    // only cleanup when call `off` or component unmount
    return [Target]
  }

  return useEventHook
}

export default createEventTargetHook