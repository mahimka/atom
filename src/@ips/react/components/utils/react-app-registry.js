import React, { useState, useEffect } from 'react'
import { register, unregister } from '@ips/app/app-registry'
import * as __ from "@ips/app/hidash"

import EventEmitter from '@ips/app/event-emitter'

export const useRegistry = (id, o) =>{
    useEffect(()=>{
        if(__.ud(id) || !__.isString(id)) return

        register(id, o)

        return ()=>unregister(id, o)

    }, [id, o])
}

export const useEventEmitter = id =>{
    const [val, setVal] = useState()
    trace('useEventEmitter', id)

    useEffect(()=>{
        if(__.ud(id) || !__.isString(id)) return

        const nval = val||{ ee: new EventEmitter() }
        setVal(nval)
        register(id, nval)
        // registerEventEmitter(id, nee)
        trace('binditrot', nval)

        return ()=>{ 
            unregister(id); 
            // unregisterEventEmitter(id, nee); 
        }

    }, [id])

    return val ? val.ee : null
}