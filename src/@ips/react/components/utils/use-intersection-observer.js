import React, { useState, useContext, useEffect, useRef } from 'react'

import { trackBox } from './vistracker'

const useIntersectionObserver = ($el, opts)=>{
    const [tracker, setTracker] = useState()
    const [visible, setVisible] = useState(true)
    useEffect(()=>{
        if(!$el) return

        const tra = trackBox($el, v=>setVisible(v), opts)
        setTracker(tra)
        return ()=>{
            tra.destroy()
            setTracker(null)
        }
    },[$el])
    
    return visible
}

export default useIntersectionObserver