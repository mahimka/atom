import { useState } from 'react';
import { useCallback } from 'use-memo-one'
import createEventTargetHook from './create-event-target-hook';

const useWindow = createEventTargetHook(window);
const useResize = () => {
  const [size, setSize] = useState({
    width: window.innerWidth,
    height: window.innerHeight
  });

  const handler = useCallback(() =>
    setSize({
      width: window.innerWidth,
      height: window.innerHeight
    }), [])

  useWindow('resize', handler);
  return size;
};
export default useResize;
