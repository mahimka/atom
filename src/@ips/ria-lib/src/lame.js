import "core-js"
// import "core-js/es6"
// import "es6-promise/dist/es6-promise.auto.js"
import ES6Promise from "es6-promise";
ES6Promise.polyfill();

import "babel-polyfill"
import "intersection-observer"
import "flexibility"
// import "web-animations-js"
import '@ips/app/matches'
import Stickyfill from "@ips/react/components/lib/stickyfilljs/stickyfill.min.js"
window.Stickyfill = Stickyfill
