import * as Metrika from '@ips/app/metrika'
// import * as GTM from '@ips/app/google-tag-manager'

// analytic events
export const connect = (opts, gopts) =>{
    // GTM.connect(opts, gopts)
    Metrika.init(opts)
}

export const entrance = ()=>{
    // GTM.event('entrance');
    Metrika.event('entrance');
}
