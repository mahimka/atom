// var webpack = require('webpack')
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// const WebpackBabelExternalsPlugin = require('webpack-babel-external-helpers-2');
var LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')

var path = require('path');
var url = require('url');

const NODE_MODULES = path.resolve(__dirname, 'node_modules')
const IPS_PACKAGES = path.resolve(__dirname, 'node_modules/@ips')
const IPS_PACKAGES_APP = path.join(IPS_PACKAGES, 'app')
// const IPS_PACKAGES_REACT = path.join(IPS_PACKAGES, 'react')
const SRC = path.resolve(__dirname, 'src')

var modulesDirs = [
  SRC,
  IPS_PACKAGES_APP, 
  path.resolve(__dirname, 'src/content'),
  path.resolve(__dirname, 'assets'),
  NODE_MODULES]//.map(path.resolve);

//var fontDir = path.resolve(__dirname, '../common')

// var upDir = path.resolve(__dirname, '..');

console.log('NODE_MODULES', NODE_MODULES)
// console.log('upDir', upDir)

var babelPlugins = {
  // dynamicImport:path.resolve(NODE_MODULES, 'babel-plugin-syntax-dynamic-import'),
  // async2gen:path.resolve(NODE_MODULES,'babel-plugin-transform-async-to-generator'),
  // jsx:path.resolve(NODE_MODULES,'babel-plugin-transform-react-jsx'),
  // lodash:path.resolve(NODE_MODULES, 'babel-plugin-lodash'),
  // restSpread:path.resolve(NODE_MODULES, 'babel-plugin-transform-object-rest-spread'),
  // reactCssModules:path.resolve(NODE_MODULES, 'babel-plugin-react-css-modules')
}
var presets = [
  '@babel/preset-env', 
  // 'babel-preset-react', 
  // 'babel-preset-es2015', 
  // 'babel-preset-stage-0'
  ].map(require.resolve);

var extensions = [ '.js', '.jsx','.es6','.styl','.css'] 

var hashCode = function(s) {
  var hash = 0, i, chr;
  if (s.length === 0) return hash;
  for (i = 0; i < s.length; i++) {
    chr   = s.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

const __times = (c, f) => (Array.from({length: c}, (_,x) => f(x)))

const prj = require('./project.json')
// fix missing prj fields
prj.title = prj.title||'[???]';
prj.description = prj.description||'[???]';
prj.riaTitle = prj.riaTitle||prj.title||'[???]';
prj.riaDescription = prj.riaDescription||prj.description||'[???]';
prj.lang = prj.lang||'ru'
prj.locale = prj.locale||'ru_RU'

var analyzerPort = 12000+(hashCode(prj.name||'none')%1000)
console.log('analyzerPort', analyzerPort)

var outPath = (process.env.NODE_ENV === 'production') ? 
  (path.resolve(__dirname, 'dist-dev')) :
  (path.resolve(__dirname, 'dist'))

// var lamePolyfills = [
//   "es6-promise/dist/es6-promise.auto.js", 
//   "babel-polyfill",
//   "intersection-observer",
//   // "web-animations-js",
//   '@ips/app/matches',
//   "@ips/react/components/lib/stickyfilljs/stickyfill", 
//   ]
//     .map(require.resolve);

const tplConfig = {
  inject:false,//'body',//'head',
  template:'src/index.ejs', // default
  lang:prj.lang,
  title:prj.title,
  excludeChunks:['lame','main','inject','index'],
  meta:{
    'viewport':'width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no',
    'author':'IPS',
    'type':'article',
    'fb:app_id':'501403387024620',
    'title':prj.title,
    'description':prj.description,
    'og:type':'article',
    'og:locale':prj.locale,
    'og:title':prj.title,
    'og:description':prj.description,
    'og:url':prj.url,
    'og:image':prj.image,
    'ria:title':prj.riaTitle,
    'ria:description':prj.riaDescription,
    'ria:image':prj.riaImage,
    'ria:image1x1':prj.riaImage1x1,
    'metrika:id':prj.metrikaId || 52449085,
  },
  scripts:[
    'https://dc.ria.ru/ips/social-extract.js',
    'https://dc.ria.ru/ips/ria-topline-black.js',
  ],
  headHtmlSnippet:`<script src="https://ria.ru/min/js/jquery/jquery-1.12.4.min.js"></script>`,
  bodyHtmlSnippet:`
    <p>illustration1</p>
    <div class="twentytwenty-container" id="wasis1">
      <img src="http://placehold.it/400x200&amp;text=pic1-was" border="0" />
      <img src="http://placehold.it/400x200&amp;text=pic1-is" border="0" />
    </div> <p>illustration2</p>
    <div class="twentytwenty-container" id="wasis2">
      <img src="http://placehold.it/400x200&amp;text=pic2-was" border="0" />
      <img src="http://placehold.it/400x200&amp;text=pic2-is" border="0" />
    </div>
    <p>the rest</p>`, // add some test content
  chunks:false,
}

const indexConfig = { ...tplConfig }
indexConfig.filename = 'index.html'
// indexConfig.scripts = tplConfig.scripts.concat(['widget-fixed-height.js'])

// const injectConfig = { ...tplConfig }
// injectConfig.filename = 'inject.html'
// injectConfig.scripts = tplConfig.scripts.concat(['inject.js'])



// const shareTplConfig = {
//   inject:false,//'body',//'head',
//   template:'src/index.ejs', // default
//   lang:prj.lang,
//   title:prj.title,
//   excludeChunks:['lame','main','inject','index'],
//   meta:{
//     'viewport':'width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no',
//     'author':'IPS',
//     'type':'article',
//     'fb:app_id':'501403387024620',
//     'metrika:id':prj.metrikaId || 52449085,
//     'title':prj.title,
//     'description':prj.description,
//     'og:type':'article',
//     'og:locale':prj.locale,
//     'og:title':prj.title,
//     'og:description':prj.description,
//     'og:url':prj.url,
//     'og:image':prj.image,
//     // 'og:image:width':'1400',
//     // 'og:image:height':'900',
//   },
//   // metarr:[{'http-equiv':"refresh",'content': `0; url=${prj.url}`}],
//   scripts:[],
//   headHtmlSnippet:`<script>window.open("${prj.url}", "_self")</script>`,
//   chunks:false,
// }

// const makeSharer= i => { 
//   const s = { ...shareTplConfig }
//   s.filename = `share${i}.html`
//   s.meta = {  // clone and modify meta
//     ...s.meta,
//     'og:image': url.resolve(prj.url, `hareimg${i}.png`),
//     'og:url': url.resolve(prj.url, s.filename),
//     // 'og:description': `${i}${i}${i}${i}${i}`
//   }
//   return s
// }

// const sharers = __times(8, makeSharer)

module.exports = {
  entry: {
    inject:['babel-regenerator-runtime','./src/index.js'],
    // inject:['babel-regenerator-runtime','./src/inject.js'],
    'widget-fixed-height':['babel-regenerator-runtime','./src/index.js'],
    'widget-responsive':['babel-regenerator-runtime','./src/index.js'],
    // lame:['./src/lame.js']//lamePolyfills,
    // 'social-extract':['@ips/app/social-extract.js'],
  },
  resolve: {
    modules:modulesDirs,
    extensions: extensions,
    enforceExtension:false,
    symlinks:false
  },
  resolveLoader: {
    //modules:,
    extensions: extensions
  },
  output: {
    path: outPath,
    filename: '[name].js',
    chunkFilename: '[name].js',
    pathinfo:true
  },
  module: {
    rules: [
      {
        test: /\.(?:js|es).?$/,
        // test:/\.js$/,
        loader: 'babel-loader',
        // exclude: /node_modules/,
        query: {
          plugins: [
            // babelPlugins.dynamicImport,
            // babelPlugins.lodash,
            // babelPlugins.restSpread,
            // babelPlugins.async2gen,
            // babelPlugins.jsx,
            // babelPlugins.reactCssModules,
            // [ // preact thing
            //   transforms.jsx, 
            //   { "pragma":"h" }
            // ]
          ],
          presets: presets
        }
      },
      {
        test: /\.(?:js|es).?$/,
        // test:/\.js$/,
        loader: 'babel-loader',
        include: [IPS_PACKAGES_APP],
        query: {
          plugins: [
            // babelPlugins.dynamicImport,
            // babelPlugins.lodash,
            // babelPlugins.restSpread,
            // babelPlugins.async2gen,
            // babelPlugins.jsx,
            // babelPlugins.reactCssModules,
            // [ // preact thing
            //   transforms.jsx, 
            //   { "pragma":"h" }
            // ]
          ],
          presets: presets
        }
      },      
      // { test: /\.json$/, loader: 'json-loader' },
      { test: /\.css$/, loaders: [ 'style-loader', 'css-loader', /*'postcss-loader'*/ ] },
      { test: /\.styl$/, loaders: [ 
        'style-loader', 
        {
          loader:'css-loader', 
          options:{
            importLoader:1,
            modules:false,
            // localIdentName:'[path]___[name]__[local]___[hash:base64:5]',
            url:false,
            // root:'.',
            // alias:{
            //   '/fonts':'./fonts'
            // }
          }
        },
        {
          loader:'stylus-loader',
          options:{
            paths:modulesDirs,
          }

        } ] },
    ]
  },          
  plugins:[
	  // new webpack.optimize.UglifyJsPlugin()
    // new webpack.IgnorePlugin(/uglify-js/)
    // new LodashModuleReplacementPlugin({
    //   'collections': true,
    //   'shorthands': true,
    //   'currying': true,
    //   'placeholders': true      
    // }),
    new HtmlWebpackPlugin(indexConfig),
    // new HtmlWebpackPlugin(injectConfig),
    // ...(sharers.map(s=>new HtmlWebpackPlugin(s)))
    // extractCSS,
    // new WebpackBabelExternalsPlugin(),
    // new BundleAnalyzerPlugin({
    //   analyzerPort: analyzerPort,
    //   openAnalyzer:false
    // }),

  ]

}