export default (opts, className, onClick)=>`
    <div class="ips-wi ${className || ''}" style="background-image:url(${opts.img})">
    
    <a class="ips-wi-link" href="${opts.url}" target="_blank" onclick="${ onClick }">
      <div class="ips-wi-body">
        <div class="ips-wi-title">${opts.title}</div>
        <div class="ips-wi-stitle">${opts.desc}</div>
        <div class="ips-wi-cta-w">
          <div class="ips-wi-cta">
            <div class="ips-wi-cta-text-w">
              <span class="ips-wi-cta-text">${opts.button}</span>
            </div>
          </div>
        </div>
      </div>
    </a>
    
    </div>`