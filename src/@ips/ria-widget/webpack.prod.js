const path = require('path');

// const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin'); //installed via npm
const HtmlWebpackPlugin = require('html-webpack-plugin');
const PluginRestSpread = require.resolve('babel-plugin-transform-object-rest-spread')
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const cssLoader = require('css-loader');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const buildPath = path.resolve(__dirname, 'dist');
let pathsToClean = [ 'dist/*.*' ]

const IPS_PACKAGES = path.resolve(__dirname, 'node_modules/@ips')

let cleanOptions = {
    root:     __dirname,
    verbose:  true,
    dry:      false
  }

module.exports = {
    devtool: false, // 'source-map',
    entry: {
        'widget-responsive': './src/widget-responsive.js',
        'widget-fixed-height': './src/inject.js',
        'inject': './src/inject.js',
    },
    output: {
        filename: '[name].js',
        // filename: 'widget-responsive.js',
        path: buildPath
    },
    node: {
        fs: 'empty'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',

                options: {
                    presets: ['env'],
                    plugins:[
                        PluginRestSpread
                    ]
                    
                }
            },
            {
                test: /\.js$/,
                include: [IPS_PACKAGES],
                loader: 'babel-loader',

                options: {
                    presets: ['env'],
                    plugins:[
                        PluginRestSpread
                    ]
                    
                }
            },            
            {
                // test: /\.(scss|css|sass)$/,
                test: /\.(css|styl)$/,
                use: [
                    // {
                        // loader: MiniCssExtractPlugin.loader
                    // },
                    {
                        // translates CSS into CommonJS
                        loader: 'css-loader',
                        options: {
                            sourceMap: false
                        }
                    },
                    {
                        // Runs compiled CSS through postcss for vendor prefixing
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: false
                        }
                    },
                    {
                        // compiles Sass to CSS
                        loader: 'stylus-loader',
                        options: {
                            sourceMap: false
                            // outputStyle: 'expanded',
                            // sourceMap: true,
                            // sourceMapContents: true
                        }
                    }
                ]
            },
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: '[name].[hash:20].[ext]',
                            limit: 8192
                        }
                    }
                ]
            }
        ]
    },
    resolve:{
        symlinks: false
    },
    plugins: [
        // new HtmlWebpackPlugin({
        //     template: './index.html',
        //     // Inject the js bundle at the end of the body of the given template
        //     inject: 'body',
        // }),
        new CleanWebpackPlugin(pathsToClean, cleanOptions),
        // new FaviconsWebpackPlugin({
        //     // Your source logo
        //     logo: './src/assets/icon.png',
        //     // The prefix for all image files (might be a folder or a name)
        //     prefix: 'icons-[hash]/',
        //     // Generate a cache file with control hashes and
        //     // don't rebuild the favicons until those hashes change
        //     persistentCache: true,
        //     // Inject the html into the html-webpack-plugin
        //     inject: true,
        //     // favicon background color (see https://github.com/haydenbleasel/favicons#usage)
        //     background: '#fff',
        //     // favicon app title (see https://github.com/haydenbleasel/favicons#usage)
        //     title: '{{projectName}}',

        //     // which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
        //     icons: {
        //         android: true,
        //         appleIcon: true,
        //         appleStartup: true,
        //         coast: false,
        //         favicons: true,
        //         firefox: true,
        //         opengraph: false,
        //         twitter: false,
        //         yandex: false,
        //         windows: false
        //     }
        // }),
        // new MiniCssExtractPlugin({
        //     filename: 'styles.[contenthash].css'
        // }),
        new OptimizeCssAssetsPlugin({
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {
                map: {
                    inline: false,
                },
                discardComments: {
                    removeAll: true
                }
            },
            canPrint: true
        })
    ]
};
