"use strict";

// Object.defineProperty(exports, "__esModule", {
//   value: true
// });
// exports.connect = connect;
// exports.post = post;
// exports.event = event;

var dataLayerName = 'dataLayer';
var dataLayer = window[dataLayerName] || [];
window[dataLayerName] = dataLayer;

(function (w, d, l, i) {
  w[l] = w[l] || [];
  w[l].push({
    'gtm.start': new Date().getTime(),
    event: 'gtm.js'
  });
  var u = 'https://www.googletagmanager.com',
      s = 'script',
      f = d.getElementsByTagName(s)[0],
      j = d.createElement(s),
      k = d.createElement('noscript'),
      m = d.createElement('iframe'),
      dl = l != 'dataLayer' ? '&l=' + l : '';
  j.async = true;
  j.src = u + '/gtm.js?id=' + i + dl;
  m.src = u + '/ns.html?id=' + i;
  m.height = "0";
  m.width = "0";
  m.style.cssText = "display:none;visibility:hidden";
  k.appendChild(m);
  f.parentNode.insertBefore(j, f);
  f.parentNode.insertBefore(k, j);
})(window, document, dataLayerName, 'GTM-T2NZ6V');

// IE Object.assign polyfill
if (!Object.assign) {
  Object.defineProperty(Object, 'assign', {
    enumerable: false,
    configurable: true,
    writable: true,
    value: function(target) {
      'use strict';
      if (target === undefined || target === null) {
        throw new TypeError('Cannot convert first argument to object');
      }

      var to = Object(target);
      for (var i = 1; i < arguments.length; i++) {
        var nextSource = arguments[i];
        if (nextSource === undefined || nextSource === null) {
          continue;
        }
        nextSource = Object(nextSource);

        var keysArray = Object.keys(Object(nextSource));
        for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
          var nextKey = keysArray[nextIndex];
          var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
          if (desc !== undefined && desc.enumerable) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }
      return to;
    }
  });
}

__ipsGlobal = __ipsGlobal||{};

__ipsGlobal.gtmApp = {};
__ipsGlobal.gtmOpts = __ipsGlobal.gtmOpts||{};

function connect(app) {
  var o = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  __ipsGlobal.gtmApp = Object.assign({}, app || {});
  __ipsGlobal.gtmApp.project = app.project || app.projectName || 'noname';
  __ipsGlobal.gtmApp.version = app.version || app.appMode || 'd';
  delete __ipsGlobal.gtmApp['projectName'];
  delete __ipsGlobal.gtmApp['appMode'];
  __ipsGlobal.gtmOpts = o;
}

function post(event) {
  console.log('gtm post', event);
  dataLayer.push(event);
}

function event(event) {
  var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  try {
    var slideName = ''; //slideName = app.location.slide || '';

    var curAnchor = ''; //app.location.anchor || '';

    var dlEvent = Object.assign({}, __ipsGlobal.gtmApp, opts);
    dlEvent.event = event; // dlEvent.anchor = curAnchor;

    // dlEvent.project = app.project || app.projectName || 'noname';
    // dlEvent.chapter_name = app.chapter_name
    // dlEvent.version = app.version || app.appMode;


    if (window.app && window.app.router) {
      dlEvent.from = app.router.getState().path;
    }

    if (event != 'pageview' && 'undefined' != typeof para) dlEvent[event] = para;

    if (event == 'generic') {
      dlEvent.object_id = para2;
    }

    if (__ipsGlobal.gtmOpts.log)
    {
      console.log('gtm.event', dlEvent);
    }

    post(dlEvent);
  } catch (err) {
    console.error(err);
  }
}