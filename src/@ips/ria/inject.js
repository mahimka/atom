(function (){

    function getParamsFromUrl(url) {
      if(!url) url = location.search;
      var query = url.substr(1);
      var result = {};
      query.split("&").forEach(function(part) {
        var item = part.split("=");
        result[item[0]] = decodeURIComponent(item[1]);
      });
      return result;
    }

    // export const parseUrlQueryParams = s => _.map(s.substr(1).split('&'), p=>p.split('='))

    var currentScript
       // = document.currentScript || document.scripts[document.scripts.length-1]

    var alls = document.querySelectorAll('script[data-uid]')
    console.log('all scripts with data-uid', alls)
    for(var i = 0; i < alls.length; i++){
      const s = alls[i]
      if(s.src.includes('ria.ru/ips/')){
        if(!s.getAttribute('data-ips-bound')){
          currentScript = s
          break
        }
        console.log(alls[i])
      }
    }
    console.log('currentScript', currentScript)
    console.log('all scripts with attr data-uid', document.querySelectorAll('script[data-uid]'))
    console.log('currentScript data-uid', currentScript.getAttribute('data-uid'))
    console.log('url uid', getParamsFromUrl()['data-uid'])

    var uid = currentScript.getAttribute('data-uid')||getParamsFromUrl()['uid']

    // var currentScript =
    //         (function() {
    //           var scripts = Array.from(document.getElementsByTagName('script'));
    //           var injectName = `${config.publicUrl}/inject.js`;
    //           var out = scripts.filter(x => x.getAttribute('src') == injectName);
    //           return out[0];
    //         })();

    // var mountPoint = currentScript.parentElement
    

  var publicPath = (function(){
      if(currentScript)
          return currentScript.src.split('/').slice(0, -1).join('/')

      return location.origin + location.pathname.replace(/\/$/, '') // remove trailing slash
  })()

  const metanp = (p, d)=> {
      var el = (d||document).querySelector(`meta[name='${ p }']`)
      if(!el)
        el = (d||document).querySelector(`meta[property='${ p }']`)
      return el && el.getAttribute('content')
  }

  function makeMetas(s){
    var el = document.createElement('div')
    el.innerHTML = s
    
    return el
  }

  fetch([publicPath, 'index.html'].join('/')).then(function(res){ return res.text() }).then(function(res){
      // console.log('have res')
      var index = makeMetas(res)
      
      // console.log('metas', 
      //   index.querySelector('title').innerText, 
      //   metanp('og:description', index),
      //   metanp('og:image', index))

      // var title = metap('og:title', index) || index.querySelector('title').innerText
      // var desc = metap('og:description', index) || metan('description', index)
      var img = metanp('ria:image', index) || metanp('og:image', index) || metanp('twitter:image', index)
      var url =  metanp('ria:url', index) || metanp('og:url', index) || metanp('twitter:url', index)

      var cci = document.createElement('div')
      cci.style.width = '100%'
      cci.style.height = '100%'
      cci.style.paddingBottom='50%'
      cci.style.backgroundImage = 'url(' + img + ')'
      cci.style.backgroundSize = 'contain'
      cci.style.backgroundPosition = 'center'
      cci.style.backgroundRepeat = 'no-repeat'

      var cca = document.createElement('a')
      cca.href = url
      cca.target= "_blank"
      cca.appendChild(cci)

      var w = cca

      var mountPoint = null
      if(uid){
        var utags = document.querySelectorAll('[data-uid="'+uid+'"]')
        // console.log('utags', utags)
        for(var i = utags.length-1; i >=0; i--)
          if(utags[i].tagName != 'SCRIPT')
            break
        mountPoint = utags[i]
      }
      else{
        // handle the case when there's no uid
        mountPoint = currentScript.parentElement

      }

      console.log('mounting widget', url, img, 'at', uid, mountPoint)
      mountPoint.appendChild(w)
  })
})()
