import './ria-topline.css'

import tpl from './ria-topline.html'


// trace('ria topline', tpl)

const el = document.createElement('div')
el.innerHTML = tpl
el.children[0].classList.add('ria_topline__black')
document.body.insertBefore(el.children[0], document.body.childNodes[0])
window.__ips_topline_ria__ = 40;