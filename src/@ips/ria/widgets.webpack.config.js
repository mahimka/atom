var webpack = require('webpack')
// var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// const WebpackBabelExternalsPlugin = require('webpack-babel-external-helpers-2');
//var LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
// var CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

var path = require('path');


const NODE_ENV = process.env.NODE_ENV
const NODE_MODULES = path.resolve(__dirname, 'node_modules')
const SRC = path.resolve(__dirname, 'src')

const pkg = require('./package.json')

var modulesDirs = [
  SRC,
  NODE_MODULES]//.map(path.resolve);

var loadersDirs = [
  NODE_MODULES
]

console.log('__dirname', __dirname)
console.log('modulesDirs', modulesDirs)

//var fontDir = path.resolve(__dirname, '../common')

// var upDir = path.resolve(__dirname, '..');

console.log('NODE_ENV = "' +  NODE_ENV  + '"')
console.log('NODE_MODULES = "' +  NODE_MODULES  + '"')
// console.log('upDir', upDir)

var babelPlugins = {
  dynamicImport:path.resolve(NODE_MODULES, 'babel-plugin-syntax-dynamic-import'),
  async2gen:path.resolve(NODE_MODULES,'babel-plugin-transform-async-to-generator'),
}

var presets = (NODE_ENV === 'production') ? 
  ['babel-preset-env', 'babel-preset-es2015', 'babel-preset-stage-0'].map(require.resolve):
  [].map(require.resolve)

var loaders = {
  babelLoader: require.resolve('babel-loader'),
  styleLoader: require.resolve('style-loader'),
  cssLoader: require.resolve('css-loader'),
  postcssLoader: require.resolve('postcss-loader'),
  stylusLoader: require.resolve('stylus-loader'),

}

console.log('presets', presets)
console.log('loaders', loaders)

var extensions = [ '.js','.es6','.styl','.css'] 

var outPath = (NODE_ENV === 'production') ? 
  (path.resolve(__dirname, 'build')) :
  (path.resolve(__dirname, 'build-dev'))


const widget = './widget-default.js'
const widgetFH = './widget-default-fixed-height.js'

const entry = 
    {
        'widget-responsive':[widget],
        'widget-fixed-height':[widgetFH],
        'inject':[widgetFH],
    }

module.exports = {
  entry,
  resolve: {
    modules:modulesDirs,
    extensions: extensions,
    enforceExtension:false,
    symlinks:false
  },    
  resolveLoader: {
    modules:loadersDirs,
    extensions: extensions
  },  
  output: {
    path: outPath,
    filename: '[name].js',
    //chunkFilename: '[name].js',
    pathinfo:true
  },
  module: {
    rules: [
      {
        test: /\.(?:js|es).?$/,
        // test:/\.js$/,
        loader: loaders.babelLoader,
        // exclude: /node_modules/,
        query: {
          plugins: [
            babelPlugins.dynamicImport,
            babelPlugins.async2gen,
          ],
          presets: presets
        }
      },
      { test: /\.json$/, loader: loaders.jsonLoader },
      { test: /\.css$/, loaders: [ loaders.styleLoader, loaders.cssLoader, /*loaders.postcssLoader*/ ] },
      { test: /\.styl$/, loaders: [ 
        loaders.styleLoader,
        {
          loader:loaders.cssLoader, 
          options:{
            importLoader:1,
            modules:false,
            // localIdentName:'[path]___[name]__[local]___[hash:base64:5]',
            url:false,
            // root:'.',
            // alias:{
            //   '/fonts':'./fonts'
            // }
          }
        },
        {
          loader:loaders.stylusLoader,
          options:{
            paths:modulesDirs,
          }

        } ] 
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
        }        
      }      
    ]
  },          
  plugins:[
    // new CaseSensitivePathsPlugin()
	  new webpack.optimize.UglifyJsPlugin()
    // new webpack.IgnorePlugin(/uglify-js/)
    // new LodashModuleReplacementPlugin({
    //   'collections': true,
    //   'shorthands': true,
    //   'currying': true,
    //   'placeholders': true      
    // }),
    // extractCSS,
    // new WebpackBabelExternalsPlugin(),
    // new BundleAnalyzerPlugin({
    //   analyzerPort: analyzerPort,
    //   openAnalyzer:false
    // }),

  ]

}