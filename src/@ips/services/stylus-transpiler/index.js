var stylus = require('stylus')

console.log("Dawarb")

const initStylusTranspiler = conn =>{
	conn.on("text", function (str) {
		// const o = JSON.parse(str)
		// console.log("parsed ", o)
		try{
			// console.log("Received ", str)
			stylus.render(str, {}, (err, res)=>{
				// console.log("compiled ", res)
				if(err)
					console.error('Error compiling Stylus:', err)
				else
					conn.sendText(res)
			})
		}catch(err){
			console.log('THROW Error compiling Stylus:', err)
		}
	})
	conn.on("close", function (code, reason) {
		console.log("Connection closed")
	})
	conn.on("error", function (error) {
		console.log(error)
	})
}

module.exports = initStylusTranspiler