var ws = require("nodejs-websocket")

// import initStylusTranspiler from '../stylus-transpiler'
// import initJsxTranspiler from '../jsx-transpiler'

// console.log("Kindus")

// var server = ws.createServer(function (conn) {
// 	console.log("New connection", conn.path)
// 	if(conn.path == '/jsx')
// 		initJsxTranspiler(conn)
// 	else if(conn.path == '/stylus')
// 		initStylusTranspiler(conn)
// }).listen(8666)

const createWsServer = (opts={})=>{
    const { services={}, port=8666 } = opts

    const scount = Object.keys(services).length
    // console.log('services', services)
    // console.log('scount', scount)

    var server = ws.createServer(function (conn) {
        console.log("New connection", conn.path)
        for(var i = 0; i < scount; i++){
            const path = Object.keys(services)[i]
            if(conn.path == path)
                return services[path](conn)
        }
    })
    server.listen(port)

    return server

}

module.exports = createWsServer

// createWsServer({
//     services:{
//         '/jsx':initJsxTranspiler,
//         '/stylus':initStylusTranspiler,
//     },
//     port:8666,
// })


/* client test code
var ws = new WebSocket('ws://localhost:8666/jsx')
ws.onmessage = e => console.log(e.data)

var tpl = `props => <Wrapper className={ props.className||'' } $val="1"/>`
var tpl = `class A extends Component{
	render(){
		const props = this.props
		return <Slice className={ props.className||'' } $val="1"/>
	}
}`

ws.send(tpl)

*/
