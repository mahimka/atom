const DOMParser = require('../../xmldom-mod').DOMParser
const XMLSerializer = require('../../xmldom-mod').XMLSerializer

const _ = require('lodash')
// console.log('parser', parser)

function kc2cc(c, upper = true){
    if(upper)
        c = '-' + c
    return c.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); })
}

const normalizeName = n=> n.replace(/\s+/g, '-').toLowerCase()

const propRole2Type = {
  'body':'text',
  'head':'text',
  'title':'text',
  'subtitle':'text',
  'desc':'text',
  'icon':'image',
  'illustration':'image',
  'sprite':'image',
  'avatar':'image',
}

function convertTemplate(template, opts){
    // var dom = parser.parseFragment(template)
    // var dom = parser.parse(template)
    // var dom = parser.parseXML(template)
    var dom = new DOMParser().parseFromString(template)
    // console.log('initial dom', dom)//JSON.stringify(dom, null, ' '))

    // opts = { ...opts, ...{ html:true, events:true, binds:true, cleanup:true } }
    opts = opts || { html:true, events:false, binds:true, cleanup:true }


    function convAttrs(node){
        if(!node.attributes && !node.attrs) return
        node.attributes = node.attributes||node.attrs
        // console.log('convAttrs', node.attributes)//.__proto__)

        for(var i = 0; i < node.attributes.length;){
            var a = node.attributes.item(i)
            // console.log(i, node.attributes.length, a.nodeName)

            if (opts.cleanup && a.nodeName == ':contenteditable'){
                // console.log('doka', i)
                node.attributes.removeNamedItem(a.nodeName)
                // node.attributes.splice(i, 1)
                // console.log('oka')
                continue;
            }

            if (a.nodeName == 'v-html'){

                if(opts.html){
                    node.appendChild(dom.createTextNode(`{@html ${ a.value } }`))
                    // node.childNodes = node.childNodes||[]
                    // node.childNodes.push({
                    //     nodeName:'#text',
                    //     value: `{{${ a.value }}}`,
                    // })
                }

                node.attributes.removeNamedItem(a.nodeName)
                // node.attributes.splice(i, 1)
                // console.log('gt', i, node.attributes)
                continue;
            }

            if (a.nodeName[0] == ':'){
                a.nodeName = a.nodeName.substr(1)
                a.name = a.nodeName

                a.value = `{ ${ a.value } }`
                a.nodeValue = a.value;

                // if(opts.binds){
                //     node.attributes.push({ name:a.nodeName.substr(1), value:`{{${ a.value }}}`})
                // }
                // node.attributes.removeNamedItem(a.nodeName)
                // node.attributes.splice(i, 1)
                // continue;
            }

            if (a.nodeName[0] == '@'){
                a.nodeName = `on:${ a.nodeName.substr(1) }`
                // if(opts.events){
                //     node.attributes.push({ name:`on:${ a.nodeName.substr(1) }`, value: a.value })
                // }
                node.attributes.removeNamedItem(a.nodeName)
                // node.attributes.splice(i, 1)
                continue;
            }

            i++
        }
    }

    function conv(node){
        // _.each(node.childNodes, n => console.log('\nn', n))
        _.each(node.childNodes, n => conv(n))

        const stdTags = ['html', 'head', 'body', 'title', 'meta', 'style', 'script', 'div', 'span', 'img', 'video', 'audio', 'source', 'a', 'p', 'i', 'br', 'slot']

        // console.log('conv', node)
        if(node.tagName && node.tagName[0] != '#' && !_.includes(stdTags, node.tagName))
            node.tagName = kc2cc(node.tagName)
        convAttrs(node)
    }

    conv(dom)
    // console.log('dom', dom)

    return new XMLSerializer().serializeToString(dom)

    // return parser.stringify(dom, 4);
    // return parser.serialize(dom)
}

function generateCode(name, props, slotted, mods, components){

    name = name.trim()
    _.each(props, p => p.name = p.name.trim())

/* ${ _.map(mods, m => m.name).join(', ') */
/* ${ _.map(mods, m => (`                if(${ m.name }) mods += ' ${ m.name }';`)).join('\n') } */

    let out = `
    import "components/${ normalizeName(name) }.css"

    import ModalImageList from "components/modal-image-list"
    import { register, unregister } from "@ips/app/app-registry"

    ${ props. length ? `   
        import { requestUrl } from '@ips/app/resource'
        import carryUnions from '@ips/typo/carry-unions'
    ` :'' }

${ _.map(components, c => `    import ${ kc2cc(c) } from "components/${ c }"`).join('\n') }

    export default {
        // tag: "${ name }",
        props:[
            ${ props.length ? (_.map(props, p=> `"${ p.name }",`).join(' ') + '\n            '):'' }"mod"
        ],
        data(){
            return {
                mod: ""
            }
        },
        oncreate(){
            if(this.get().name)
                register(this.get().name, this)

            this.set({
            ${ _.filter( props, p =>( propRole2Type[p.role]=='image' ))
                .map( p =>
`               "${ p.name }": requestUrl(this.get()["${ p.name }"], "image")`).join(',\n') }
            })

            this.set({
            ${ _.filter( props, p =>( propRole2Type[p.role]=='text' ))
                .map( p =>
`               "${ p.name }": (this.get()["${ p.name }"] ? carryUnions(this.get()["${ p.name }"]) : '')`).join(',\n') }
            })
        },
        ondestroy(){
            if(this.get().name)
                unregister(this.get().name, this)
        },
        computed:{
            fullClassname:({ mod })=>{ var c = "${ name }"; if(mod) c += mod.split(' ').map(s => \` ${ name }_\$\{ s \}\`).join(' '); return c; },
        },
        methods:{
        },
        components:{
            ModalImageList,
${ _.map(components, c => `            ${ kc2cc(c) }`).join(',\n') }
        }
    }
`
    return out;
}

function generatePageCode(components){

    let out = `
${ _.map(components, c => `    import ${ kc2cc(c) } from "components/${ c }"`).join('\n') }



    export default {
        components:{
${ _.map(components, c => `            ${ kc2cc(c) }`).join(',\n') }
        }
    }
`
    return out;
}

function collectTags(html, excludeStdTags = true, excludeExtra = []) {
  const stdTags = ['html', 'head', 'body', 'title', 'meta', 'style', 'script', 'div', 'span', 'img', 'video', 'audio', 'source', 'a', 'p', 'i', 'strong', 'b', 'ul', 'li', 'h1', 'h2', 'h3', 'br', 'slot']

  // var doc = document.implementation.createHTMLDocument('');
  // doc.body.innerHTML = html;

  var doc = new DOMParser().parseFromString(html)

  var am = new Set();

  function tra(node) {
    // console.log('tra', node)
    if (node.tagName)
      am.add(node.tagName.toLowerCase())

    _.each(node.childNodes, n => tra(n))
  }

  tra(doc)

  if (excludeStdTags) {
    am.forEach(s => {
      if (_.includes(stdTags, s) || _.includes(excludeExtra, s))
        am.delete(s)
    })
  }

  return Array.from(am);
}

module.exports = {
    convertTemplate,
    generateCode,
    generatePageCode,
    collectTags
}

