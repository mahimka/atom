import React from 'react';
import html from 'react-inner-html';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'
import { carryUnions } from '../../utils/typo.js'

export default class About extends React.Component {
  constructor(props) {
    super(props);

    this.state={}
  }

  componentDidMount() {

  }

  render() {
    return (
      <div className={`atom-about` + ((this.props.visible)?` atom-about-visible`:``)}>
        <div className={`atom-about-name`}>{`ФОТОПРОЕКТ`}</div>
        <div className={`atom-about-header`}>{`ЛЮДИ — ЛЮДЯМ`}</div>
        
        <div className={`atom-text-block`}>
          <div className={`atom-about-text`} data-id={"0"}>{carryUnions(`Уличное и домашнее освещение, вода из крана, возможность быстро передвигаться по всему миру на поездах, самолётах, кораблях, питаться продуктами, привезёнными из разных стран, — эти и многие другие радости цивилизации уже прочно вошли в нашу жизнь.`)}</div>
          <div className={`atom-about-text`} data-id={"1"}>{carryUnions(`Между тем, за каждым таким обыденным явлением стоят тысячи людей, которые вкладывают частицу своего труда в наш комфорт. Это учёные, инженеры, конструкторы, технологи, рабочие, энергетики и многие другие. Благодаря им круглосуточно работают атомные и тепловые электростанции, ходят корабли и поезда, доставляются грузы, создаётся основное оборудование для важнейших отраслей промышленности.`)}</div>
          <div className={`atom-about-text`} data-id={"2"}>{carryUnions(`«ЛЮДИ — ЛЮДЯМ» — это семь фотосюжетов о людях из самых разных сфер машиностроения. Это демонстрация непрерывного взаимообмена человеческой энергией. Каждая история наглядно покажет, кто создаёт привычный нам мир. А фотографии передадут характер и эмоции мастеров своего дела, чтобы вы смогли увидеть настоящий и живой портрет промышленности в лицах.`)}</div>
        </div>

        <div 
          className={`atom-about-cross`} 
          {...html(require(`!raw-loader!../../assets/cross.svg`))}
          onClick={()=>{
            this.props.setAbout(false);
          }}
        />

      </div>
    );
  }
}
