import React, { Fragment, Suspense, lazy } from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap, EasePack, ScrollToPlugin } from 'gsap/all'
import smoothscroll from 'smoothscroll-polyfill';

import Folder from '../folder';
import S0 from '../slide0';
import S1 from '../slide1';
import S2 from '../slide2';
import S3 from '../slide3';
import S4 from '../slide4';
import S5 from '../slide5';
import S6 from '../slide6';
import S7 from '../slide7';
import Footer from '../footer';

import Menu from '../menu';
import About from '../about';
import Stories from '../stories';
import Start from '../start';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    gsap.defaults({overwrite: "true"});
    gsap.registerPlugin(ScrollToPlugin);
    smoothscroll.polyfill();

    this.state = {
      about: false, 
      stories: false,
      blackmode: false,
      start: true,
      mobile: false,
      storie: 0
    }
    this.setAbout = this.setAbout.bind(this);
    this.setStories = this.setStories.bind(this);
    this.setStart = this.setStart.bind(this);
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
    this.handleWaypointLeave = this.handleWaypointLeave.bind(this);
    this.handleResize = this.handleResize.bind(this);
  }

  componentDidMount() {
    // resize
    window.addEventListener('resize', this.handleResize);
    this.handleResize();
    setTimeout(this.handleResize, 1000);
  }

  handleResize() {
    this.setState({mobile: window.innerWidth < 769})
  }


  setAbout(val){
    this.setState({about: val, stories: false})
  }
  setStories(val){
    this.setState({stories: val})
  }
  setStart(val, i){
    this.setState({start: val, storie: i, stories: false})

    if(!val){
        let elem = document.querySelector(`.atom-section[data-id="folder"]`);
        let off = 0
        gsap.to(window, {duration: 0, scrollTo: {y:elem, offsetY: off}, ease: "power2.inOut"});
    }

  }


  handleWaypointEnter(props){
    this.setState({blackmode: true})
  }
  handleWaypointLeave(props){
    this.setState({blackmode: false})
  }

  render() {
    const data = this.props.data.sections[this.state.storie];
    return (
      <div className={`atom-container` + ((this.state.blackmode)?` atom-container-black`:``)}>
            
            {
              (this.state.start) ? <Fragment>
                <Start setStart={this.setStart} mobile={this.state.mobile}/>
              </Fragment>

              :

              <Fragment>
                <Folder mobile={this.state.mobile} storie={this.state.storie} h1={data.h1}/>
                <S0 data={data.sections[0]} mobile={this.state.mobile} storie={this.state.storie}/>

                <Waypoint scrollableAncestor={window} bottomOffset={"50%"} topOffset={"0%"} fireOnRapidScroll={false}
                  onEnter={(props) => {this.handleWaypointEnter(props)}}
                  onLeave={(props) => {this.handleWaypointLeave(props)}}
                >
                  <div>
                    <S1 data={data.sections[1]} mobile={this.state.mobile} storie={this.state.storie} />
                    <S2 data={data.sections[2]} mobile={this.state.mobile} storie={this.state.storie} />
                    <S3 data={data.sections[3]} mobile={this.state.mobile} storie={this.state.storie} />
                    <S4 data={data.sections[4]} mobile={this.state.mobile} storie={this.state.storie} />
                    <S5 data={data.sections[5]} mobile={this.state.mobile} storie={this.state.storie} />
                  </div>
                </Waypoint>
                
                <S6 data={data.sections[6]} mobile={this.state.mobile} storie={this.state.storie} />
                <S7 data={data.sections[7]} mobile={this.state.mobile} storie={this.state.storie} />
                <Footer mobile={this.state.mobile} setStart={this.setStart} mobile={this.state.mobile} storie={this.state.storie} h1={data.h2}/>
              </Fragment>
            }

            <Menu setAbout={this.setAbout} setStories={this.setStories} start={this.state.start} mobile={this.state.mobile}/>
            <About visible={this.state.about} setAbout={this.setAbout}/>
            <Stories visible={this.state.stories} setStories={this.setStories} setStart={this.setStart}/>
      </div>
    );
  }
}
