import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'

const sizes = [948.41, 1066.72, 1146.86, 886.92, 1299.58, 886.92, 1563.67];
const msizes = [239, 270.92, 291.27, 225.25, 330.06, 225.25, 397.13]


export default class Folder extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {
    if(!this.props.mobile){
      gsap.to(`.atom-folder-top-texts`,{x:(-sizes[this.props.storie] - 60), repeat: -1, duration: 6, ease: "none" })
      gsap.set(`.atom-folder-bottom-texts`,{x:(-sizes[this.props.storie] - 60)})
      gsap.to(`.atom-folder-bottom-texts`,{x:0, repeat: -1, duration: 6, ease: "none" })
    }else{
      gsap.to(`.atom-folder-top-texts`,{x:(-msizes[this.props.storie] - 60), repeat: -1, duration: 6, ease: "none" })
      gsap.set(`.atom-folder-bottom-texts`,{x:(-msizes[this.props.storie] - 60)})
      gsap.to(`.atom-folder-bottom-texts`,{x:0, repeat: -1, duration: 6, ease: "none" })
    }
  }


  componentDidUpdate(prevProps, prevState, snapshot){
    if( (prevProps.mobile != this.props.mobile) || (prevProps.storie != this.props.storie) ){
      if(!this.props.mobile){
        gsap.killTweensOf([`.atom-folder-top-texts`, `.atom-folder-bottom-texts`]);
        gsap.set(`.atom-folder-top-texts`,{x:0})
        gsap.set(`.atom-folder-bottom-texts`,{x:0})
        
        gsap.to(`.atom-folder-top-texts`,{x:(-sizes[this.props.storie] - 60), repeat: -1, duration: 6, ease: "none" })
        gsap.set(`.atom-folder-bottom-texts`,{x:(-sizes[this.props.storie] - 60)})
        gsap.to(`.atom-folder-bottom-texts`,{x:0, repeat: -1, duration: 6, ease: "none" })
      }else{
        gsap.killTweensOf([`.atom-folder-top-texts`, `.atom-folder-bottom-texts`]);
        gsap.set(`.atom-folder-top-texts`,{x:0})
        gsap.set(`.atom-folder-bottom-texts`,{x:0})
        
        gsap.to(`.atom-folder-top-texts`,{x:(-msizes[this.props.storie] - 60), repeat: -1, duration: 6, ease: "none" })
        gsap.set(`.atom-folder-bottom-texts`,{x:(-msizes[this.props.storie] - 60)})
        gsap.to(`.atom-folder-bottom-texts`,{x:0, repeat: -1, duration: 6, ease: "none" })
      }
    }

    if(prevProps.storie != this.props.storie){
      gsap.killTweensOf([`.atom-folder-image-container`]);
      gsap.set(`.atom-folder-image-container`,{opacity:0, y: 30})
      gsap.to(`.atom-folder-image-container`,{y:0, opacity:1, duration: 1, ease: "Power2.easeOut", delay: 0.5 })
    }

  }

  handleWaypointEnter(props){

  }
  handleWaypointLeave(props){

  }

  render() {
    const h1 = this.props.h1;
    const storie = this.props.storie;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`atom-section`} data-id={"folder"}>
          
          <div className={`atom-folder-texts`}>
            <div className={`atom-folder-texts-line`} />
            <div className={`atom-folder-top-texts`}>
              <span className={`atom-folder-top-text`}>{h1}</span>
              <span className={`atom-folder-top-text`}>{h1}</span>
              <span className={`atom-folder-top-text`}>{h1}</span>
            </div>
            <div className={`atom-folder-bottom-texts`}>
              <span className={`atom-folder-top-text`}>{h1}</span>
              <span className={`atom-folder-top-text`}>{h1}</span>
              <span className={`atom-folder-top-text`}>{h1}</span>
            </div>
          </div>

          <div className={`atom-folder-image-container`}>
            <img key={`img/${storie}/${storie}_0.jpg`} src={`img/${storie}/${storie}_0.jpg`} srcSet={`img/${storie}/${storie}_0_2x.jpg 2x`} className={`atom-folder-img`} data-id={"0-1"}/>
          </div>

          <div className={`atom-folder-arr`} {...html(require(`!raw-loader!../../assets/arrowm.svg`))} />

        </section>
      </Waypoint>
    );
  }
}
