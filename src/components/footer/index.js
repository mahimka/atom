import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import {EasePack, ScrollToPlugin} from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'

const sizes = [583.2, 641.77, 485.13, 689.45, 506, 837.69, 526.56];
const msizes = [270.92, 291.27, 225.25, 330.06, 225.25, 397.13, 240.88]


export default class Footer extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {

  }


  componentDidMount() {
    if(!this.props.mobile){
      gsap.to(`.atom-footer-top-texts`,{x:(-sizes[this.props.storie] - 60), repeat: -1, duration: 6, ease: "none" })
      gsap.set(`.atom-footer-bottom-texts`,{x:(-sizes[this.props.storie] - 60)})
      gsap.to(`.atom-footer-bottom-texts`,{x:0, repeat: -1, duration: 6, ease: "none" })
    }else{
      gsap.to(`.atom-footer-top-texts`,{x:(-msizes[this.props.storie] - 60), repeat: -1, duration: 6, ease: "none" })
      gsap.set(`.atom-footer-bottom-texts`,{x:(-msizes[this.props.storie] - 60)})
      gsap.to(`.atom-footer-bottom-texts`,{x:0, repeat: -1, duration: 6, ease: "none" })
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot){
    if(prevProps.mobile != this.props.mobile || (prevProps.storie != this.props.storie)){
      if(!this.props.mobile){
        gsap.killTweensOf([`.atom-footer-top-texts`, `.atom-footer-bottom-texts`]);
        gsap.set(`.atom-footer-top-texts`,{x:0})
        gsap.set(`.atom-footer-bottom-texts`,{x:0})
        
        gsap.to(`.atom-footer-top-texts`,{x:(-sizes[this.props.storie] - 60), repeat: -1, duration: 6, ease: "none" })
        gsap.set(`.atom-footer-bottom-texts`,{x:(-sizes[this.props.storie] - 60)})
        gsap.to(`.atom-footer-bottom-texts`,{x:0, repeat: -1, duration: 6, ease: "none" })
      }else{
        gsap.killTweensOf([`.atom-footer-top-texts`, `.atom-footer-bottom-texts`]);
        gsap.set(`.atom-footer-top-texts`,{x:0})
        gsap.set(`.atom-footer-bottom-texts`,{x:0})
        
        gsap.to(`.atom-footer-top-texts`,{x:(-msizes[this.props.storie] - 60), repeat: -1, duration: 6, ease: "none" })
        gsap.set(`.atom-footer-bottom-texts`,{x:(-msizes[this.props.storie] - 60)})
        gsap.to(`.atom-footer-bottom-texts`,{x:0, repeat: -1, duration: 6, ease: "none" })
      }
    }
  }

  handleWaypointEnter(props){

  }
  handleWaypointLeave(props){

  }

  render() {
    const data = this.props.data;

    const h1 = this.props.h1;
    const storie = this.props.storie;

    let nextstorie = storie + 1
    if(nextstorie > 6)  nextstorie = 0

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`atom-section`} data-id={"footer"}>

          <div className={`atom-footer-top`}

            onClick={()=>{
              this.props.setStart(false, nextstorie);
            }}

          >
            <div className={`atom-footer-texts`}>
              <div className={`atom-footer-texts-line`} />
              <div className={`atom-footer-top-texts`}>
                <span className={`atom-footer-top-text`}>{h1}</span>
                <span className={`atom-footer-top-text`}>{h1}</span>
                <span className={`atom-footer-top-text`}>{h1}</span>
                <span className={`atom-footer-top-text`}>{h1}</span>
              </div>
              <div className={`atom-footer-bottom-texts`}>
                <span className={`atom-footer-top-text`}>{h1}</span>
                <span className={`atom-footer-top-text`}>{h1}</span>
                <span className={`atom-footer-top-text`}>{h1}</span>
                <span className={`atom-footer-top-text`}>{h1}</span>
              </div>
            </div>

            <div className={`atom-footer-arrow`}>
              <div className={`atom-next-link-box-text0`}>{`Следующий сюжет`}</div>
              <div className={`atom-next-link-box-text1`}>{h1}</div>
              <div className={`atom-next-link-arrow`} {...html(require(`!raw-loader!../../assets/right-arrow${(storie==5)?"s":""}.svg`))} />
            </div>

          <div className={`atom-footer-image-container`}>
            <img 
              src={ (this.props.mobile) ? `img/${storie}/mob_${storie}_16.jpg` : `img/${storie}/${storie}_16.jpg` } 
              srcSet={ (this.props.mobile) ? `img/${storie}/mob_${storie}_16.jpg 2x` : `img/${storie}/${storie}_16_2x.jpg 2x` }
              className={`atom-footer-img`}
            />
          </div>

          </div>

          <div className={`atom-footer-bottom`}>
            <div className={`atom-footer-line`} />
            <div className={`atom-bottom-row`}>
              <div 
                className={`atom-bottom-logo`} 
                {...html(require(`!raw-loader!../../assets/logo.svg`))} 
                onClick={()=>{
                  window.open("http://www.aem-group.ru/", "_blank");
                }}
              />

              <div className={`atom-bottom-txt-box`}>
                <div className={`atom-bottom-txt`} dangerouslySetInnerHTML={{__html: `Проект подготовлен при участии АО «Атомэнергомаш»` }} data-id={"0"} />
                <div className={`atom-bottom-txt`} dangerouslySetInnerHTML={{__html: `МИА «Россия сегодня», 2020 год` }} data-id={"1"} />
                <div className={`atom-bottom-txt`} dangerouslySetInnerHTML={{__html: `Фотографии и видео предоставлены пресс-службой АО «Атомэнергомаш»` }} data-id={"2"} />
              </div>

              <div className={`atom-social`}>
                
                <div 
                  className={`atom-bottom-logo-mobile`} 
                  {...html(require(`!raw-loader!../../assets/logomb.svg`))} 
                  onClick={()=>{
                    window.open("http://www.aem-group.ru/", "_blank");
                  }}
                />

                <div 
                  className={`atom-social-btn`} 
                  onClick={()=>{
                    window.open("https://ru-ru.facebook.com/aemgroup", "_blank");
                  }}
                  data-id={"0"}
                  {...html(require(`!raw-loader!../../assets/social0.svg`))} />
                
                <div 
                className={`atom-social-btn`} 
                onClick={()=>{
                  window.open("https://www.instagram.com/atomenergomash/", "_blank");
                }}
                data-id={"1"} {...html(require(`!raw-loader!../../assets/social1.svg`))} />
                
                <div 
                className={`atom-social-btn`}
                onClick={()=>{
                  window.open("https://vk.com/atomenergomash", "_blank");
                }}
                data-id={"2"} 
                {...html(require(`!raw-loader!../../assets/social2.svg`))} />
              </div>
            </div>
          </div>

        </section>
      </Waypoint>
    );
  }
}
