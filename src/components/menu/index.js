import React from 'react';
import html from 'react-inner-html';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'


export default class Menu extends React.Component {
  constructor(props) {
    super(props);

    this.state={}
  }

  componentDidMount() {

  }

  render() {
    const data = this.props.data;

    return (
      <div className={`atom-menu`}>
        <div 
          className={`atom-menu-logo`} 
          {...html(require(`!raw-loader!../../assets/logo${(this.props.mobile)?`m`:``}.svg`))}
          onClick={()=>{
            window.open("http://www.aem-group.ru/", "_blank");
          }}
        />
        
        <div 
          className={`atom-menu-about`}
          onClick={()=>{
            this.props.setAbout(true);
          }}
        >{`О ПРОЕКТЕ`}<div className={`atom-menu-line`} data-id={"0"}/></div>
        

        {
          (this.props.start)? <div className={`atom-menu-stories atom-menu-stories-start`} /> :

          <div 
            className={`atom-menu-stories`}
            onClick={()=>{
              this.props.setStories(true);
            }}
            >
            
            <div className={`atom-menu-stories-txt`} data-id={"1"}>{`СЮЖЕТЫ`}<div className={`atom-menu-line`} data-id={"1"}/></div>
            <div className={`atom-menu-stories-burger`} {...html(require(`!raw-loader!../../assets/burger.svg`))} />

            </div>
        }


      
      </div>
    );
  }
}
