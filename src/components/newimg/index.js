import React, { useRef } from 'react'
import html from 'react-inner-html';

import { Waypoint } from 'react-waypoint';
import { gsap, EasePack, CSSRulePlugin } from 'gsap/all'

export default class Newimg extends React.Component {
  constructor(props) {
    super(props);
    gsap.defaults({overwrite: "true"});
    gsap.registerPlugin(CSSRulePlugin);
    this.state = {

    }

    this.myRef = React.createRef();
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
    this.handleWaypointLeave = this.handleWaypointLeave.bind(this);
  }

  componentDidMount() {
    this.handleWaypointLeave();
  }

  componentDidUpdate(prevProps, prevState, snapshot){

  }

  handleWaypointEnter(props){
    if(!this.props.mobile){
        
        let dx=-30;
        let dy=20;
        if(this.props.id=="2-0") {dx=-60; dy=-40;}
        if(this.props.id=="3-0") {dx=60; dy=40;}
        if(this.props.id=="4-0") {dx=-60; dy=40;}
        if(this.props.id=="6-0") {dx=60; dy=40;}
        if(this.props.id=="7-0") {dx=-40; dy=20;}

        let rule = CSSRulePlugin.getRule(`.atom-small-image-container[data-id="${this.props.id}"]::before`);
        gsap.killTweensOf([rule]);
        gsap.set(rule, {cssRule: {x:dx, y:dy} });
        gsap.to(rule, {cssRule: {x:0, y:0}, duration: 1.0, ease: "power2.out"});

        let rule2 = CSSRulePlugin.getRule(`.atom-small-image-container[data-id="${this.props.id}"]`);
        gsap.killTweensOf([rule2]);
        gsap.set(rule2, {cssRule: {opacity:0} });
        gsap.to(rule2, {cssRule: {opacity:1}, duration: 3, ease: "power2.out"});

        if(this.props.tid != "2-0"){
          let rule3 = document.querySelector(`.atom-alt[data-id="${this.props.tid}"]`);
          gsap.killTweensOf([rule3]);
          gsap.set(rule3, {opacity:0, y:15} );
          gsap.to(rule3, {opacity:1, y:0, duration: 1.2, ease: "power2.out"});
        }

    }
  }

  handleWaypointLeave(props){
      if(!this.props.mobile){
      // let rule = CSSRulePlugin.getRule(`.atom-small-image-container[data-id="${this.props.id}"]::before`);
      // gsap.killTweensOf([rule]);
      // gsap.to(rule, {cssRule: {x:-31, y:20}, duration: 0.3, ease: "power2.out"});

      let rule2 = CSSRulePlugin.getRule(`.atom-small-image-container[data-id="${this.props.id}"]`);
      gsap.killTweensOf([rule2]);
      gsap.to(rule2, {cssRule: {opacity:0}, duration: 0.3, ease: "power2.out"});

      if(this.props.id != "2-0"){
        let rule3 = document.querySelector(`.atom-alt[data-id="${this.props.tid}"]`);
        gsap.killTweensOf([rule3]);
        gsap.to(rule3, {opacity:0, duration: 0.3, ease: "power2.out"});
      }
    }
  }

  render() {

    const id = this.props.id
    const tid = this.props.tid
    const src = this.props.src
    const srcset = this.props.srcset
    const alt = this.props.alt

    return (
      <Waypoint
        scrollableAncestor={window}
        bottomOffset={"30%"}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
        fireOnRapidScroll={false}
      >
      <div>
        <div className={`atom-small-image-container`} data-id={id}>
          <img 
            src={src} 
            srcSet={srcset}
            className={`atom-small-img`} 
            alt={alt}
            data-id={id}
          />
        </div>
        {
          (this.props.tid != "2-0") ? <div className={`atom-alt`} data-id={tid}>{alt}</div> : <div/>
        }
      </div>
      </Waypoint>
    );
  }
}