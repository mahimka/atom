import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'

import { carryUnions } from '../../utils/typo.js'


import Newimg from '../newimg'

export default class Slide0 extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {

  }


  handleWaypointEnter(props){

  }
  handleWaypointLeave(props){

  }

  render() {
    const data = this.props.data;
    const storie = this.props.storie;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`atom-section`} data-id={"0"}>
          <div className={`atom-row`} data-id={"0-0"}>
            <div className={`atom-col`} data-id={"0-0"}>
              
              <div className={`atom-text-m`} data-id={"0-0"}>{carryUnions(data.txt[0])}</div>

              <Newimg 
                id={"0-0"} 
                tid={"0-0"} 
                src={`img/${storie}/${storie}_${data.imgs[0].id}.jpg`}
                srcset={`img/${storie}/${storie}_${data.imgs[0].id}_2x.jpg 2x`}
                alt={carryUnions(data.imgs[0].alt)}
                mobile={this.props.mobile} 
              />

              
              
              <div className={`atom-text-m`} data-id={"0-1"}>{carryUnions(data.txt[1])}</div>

              <Newimg 
                id={"0-1"} 
                tid={"0-1"} 
                src={`img/${storie}/${storie}_${data.imgs[1].id}.jpg`}
                srcset={`img/${storie}/${storie}_${data.imgs[1].id}_2x.jpg 2x`}
                alt={carryUnions(data.imgs[1].alt)}
                mobile={this.props.mobile}  
              />

              <div className={`atom-text-m`} data-id={"0-2"}>{carryUnions(data.txt[2])}</div>

              <div className={`atom-text-m`} data-id={"0-3"}>{(storie == 1 || storie == 2)?carryUnions(data.txt[3]):''}</div>

            </div>
            <div className={`atom-col`} data-id={"0-1"}>

              {
                data.txt.map((d, i)=>(
                  <div 
                    className={`atom-text`}
                    data-id={`0-${i}`}
                    key={i}
                  >
                    {carryUnions(d)}
                  </div>
                ))
              }

            </div>
          </div>
        </section>
      </Waypoint>
    );
  }
}
