import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'
import { carryUnions } from '../../utils/typo.js'
import Newimg from '../newimg'
export default class Slide2 extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
  }

  componentDidMount() {

  }


  handleWaypointEnter(props){

  }
  handleWaypointLeave(props){

  }

  render() {
    const data = this.props.data;
    const storie = this.props.storie;

    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"50%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`atom-section`} data-id={"2"}>
          
          <div className={`atom-row`}>
            <div className={`atom-col`} data-id={"2-0"}>



              <Newimg 
                id={"2-0"} 
                tid={"2-0"} 
                src={`img/${storie}/${storie}_${data.img}.jpg`}
                srcset={`img/${storie}/${storie}_${data.img}_2x.jpg 2x`}
                alt={data.txt}
                mobile={this.props.mobile} 
              />



            </div>
            <div className={`atom-col`} data-id={"2-1"}>
              <div className={`atom-text`} data-id={"2-0"}>{carryUnions(data.txt)}</div>
            </div>
          </div>

        </section>
      </Waypoint>
    );
  }
}
