import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'

import Lottie from 'react-lottie-segments';
import { carryUnions } from '../../utils/typo.js'

export default class Slide5 extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false,
      json: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
        this.handleWaypointEnter_a = this.handleWaypointEnter_a.bind(this);
    this.handleWaypointLeave_a = this.handleWaypointLeave_a.bind(this);

    this.loadJSON = this.loadJSON.bind(this);
  }

  componentDidMount() {
    this.loadJSON();
     this.handleWaypointLeave_a();
  }
  componentDidUpdate(prevProps, prevState, snapshot){
    if(prevProps.storie != this.props.storie){
      this.loadJSON();
    }
  }

  loadJSON(){
    const storie = this.props.storie + 1;
    import(`../../../static/data/${storie}/${storie}_3/${storie}_3.json`).then(json => {
      this.setState({ json: json })
     })
  }


  handleWaypointEnter(props){

  }
  handleWaypointLeave(props){

  }

  handleWaypointEnter_a(props){
    if(!this.props.mobile){
        let rule3 = document.querySelector(`.atom-alt[data-id="5-0"]`);
        gsap.killTweensOf([rule3]);
        gsap.set(rule3, {opacity:0, y:15} );
        gsap.to(rule3, {opacity:1, y:0, duration: 1.2, ease: "power2.out"});
    }
  }

  handleWaypointLeave_a(props){
      if(!this.props.mobile){
        let rule3 = document.querySelector(`.atom-alt[data-id="5-0"]`);
        gsap.killTweensOf([rule3]);
        gsap.to(rule3, {opacity:0, duration: 0.3, ease: "power2.out"});
    }
  }


  render() {
    const data = this.props.data;
    const storie = this.props.storie;
    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`atom-section`} data-id={"5"}>
          <div className={`atom-line`} data-id={4}/>

          <img 
            src={`img/${storie}/${storie}_${data.img}.jpg`} 
            srcSet={`img/${storie}/${storie}_${data.img}_2x.jpg 2x`}
            className={`atom-big-img`} 
            alt={data.alt}
            data-id={"5"}
          />


          <div className={`atom-row`}>
            <div className={`atom-col`}>
            <Waypoint
              scrollableAncestor={window}
              bottomOffset={"30%"}
              onEnter={(props) => {this.handleWaypointEnter_a(props)}}
              onLeave={(props) => {this.handleWaypointLeave_a(props)}}
              fireOnRapidScroll={false}
            >
              <div className={`atom-alt`} data-id={"5-0"}>{carryUnions(data.alt)}</div>
            </Waypoint>
            </div>
            <div className={`atom-col`}>
              <div className={`atom-text`} data-id={"5-0"}>{carryUnions(data.txt)}</div>
            </div>
          </div>

        <div className={`atom-big-sheme`} data-id={"5-0"}>

            {
              (this.state.json) 

                ?
            <Lottie 
                options={{
                    loop: true,
                    autoplay: true, 
                    animationData: this.state.json,
                    rendererSettings: { preserveAspectRatio: 'xMidYMid slice'}
                }}
                width={this.props.mobile?600:1400}
                height={this.props.mobile?180:420}
                isStopped={false}
                isPaused={false}
            />

              :

            <div/>

            }
        </div>
        </section>
      </Waypoint>
    );
  }
}
