import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'
import { carryUnions } from '../../utils/typo.js'
import Newimg from '../newimg'
export default class Slide6 extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
        this.handleWaypointEnter_a = this.handleWaypointEnter_a.bind(this);
    this.handleWaypointLeave_a = this.handleWaypointLeave_a.bind(this);

  }

  componentDidMount() {
this.handleWaypointLeave_a();
  }


  handleWaypointEnter(props){

  }
  handleWaypointLeave(props){

  }

  handleWaypointEnter_a(props){
    if(!this.props.mobile){
        let rule3 = document.querySelector(`.atom-alt[data-id="6-0"]`);
        gsap.killTweensOf([rule3]);
        gsap.set(rule3, {opacity:0, y:15} );
        gsap.to(rule3, {opacity:1, y:0, duration: 1.2, ease: "power2.out"});
    }
  }

  handleWaypointLeave_a(props){
      if(!this.props.mobile){
        let rule3 = document.querySelector(`.atom-alt[data-id="6-0"]`);
        gsap.killTweensOf([rule3]);
        gsap.to(rule3, {opacity:0, duration: 0.3, ease: "power2.out"});
    }
  }


  render() {
    const data = this.props.data;
    const storie = this.props.storie;
    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`atom-section`} data-id={"6"}>
          <div className={`atom-line`} data-id={3}/>

          <img 
            src={`img/${storie}/${storie}_${data.img}.jpg`} 
            srcSet={`img/${storie}/${storie}_${data.img}_2x.jpg 2x`}
            className={`atom-big-img`} 
            alt={data.alt}
            data-id={"4"}
          />

          <div className={`atom-row`}>
            <div className={`atom-col`}>
            <Waypoint
              scrollableAncestor={window}
              bottomOffset={"30%"}
              onEnter={(props) => {this.handleWaypointEnter_a(props)}}
              onLeave={(props) => {this.handleWaypointLeave_a(props)}}
              fireOnRapidScroll={false}
            >
              <div className={`atom-alt`} data-id={"6-0"}>{carryUnions(data.alt)}</div>
            </Waypoint>
            </div>
            <div className={`atom-col`}>
              
            </div>
          </div>
          <div className={`atom-row`} data-id={"3-0"}>
            <div className={`atom-col`} data-id={"3-0"}>
              <div className={`atom-text`} data-id={"3-0"}>{carryUnions(data.txt)}</div>
            </div>
            <div className={`atom-col`} data-id={"3-1"}>
              
              <Newimg 
                id={"6-0"} 
                tid={"6-1"} 
                src={`img/${storie}/${storie}_${data.imgr}.jpg`}
                srcset={`img/${storie}/${storie}_${data.imgr}_2x.jpg 2x`}
                alt={carryUnions(data.altr)}
                mobile={this.props.mobile} 
              />

            </div>
          </div>

        </section>
      </Waypoint>
    );
  }
}
