import React, { Fragment } from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'
import { carryUnions } from '../../utils/typo.js'
import Newimg from '../newimg'
export default class Slide7 extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fired: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
        this.handleWaypointEnter_a = this.handleWaypointEnter_a.bind(this);
    this.handleWaypointLeave_a = this.handleWaypointLeave_a.bind(this);

  }

  componentDidMount() {
this.handleWaypointLeave_a();
  }


  handleWaypointEnter(props){

  }
  handleWaypointLeave(props){

  }

  handleWaypointEnter_a(props){
    if(!this.props.mobile){
        let rule3 = document.querySelector(`.atom-alt[data-id="7-0"]`);
        gsap.killTweensOf([rule3]);
        gsap.set(rule3, {opacity:0, y:15} );
        gsap.to(rule3, {opacity:1, y:0, duration: 1.2, ease: "power2.out"});
    }
  }

  handleWaypointLeave_a(props){
      if(!this.props.mobile){
        let rule3 = document.querySelector(`.atom-alt[data-id="7-0"]`);
        gsap.killTweensOf([rule3]);
        gsap.to(rule3, {opacity:0, duration: 0.3, ease: "power2.out"});
    }
  }


  render() {
    const data = this.props.data;
    const storie = this.props.storie;



    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`atom-section`} data-id={"7"}>
          <div className={`atom-line`} />
          
          <img 
            src={`img/${storie}/${storie}_${data.img}.jpg`} 
            srcSet={`img/${storie}/${storie}_${data.img}_2x.jpg 2x`}
            className={`atom-big-img`} 
            alt={data.alt}
            data-id={"7"}
          />


          {
            (storie != 1) 
            ?
            <div className={`atom-row`}>
              <div className={`atom-col`}>
            <Waypoint
              scrollableAncestor={window}
              bottomOffset={"30%"}
              onEnter={(props) => {this.handleWaypointEnter_a(props)}}
              onLeave={(props) => {this.handleWaypointLeave_a(props)}}
              fireOnRapidScroll={false}
            >
              <div className={`atom-alt`} data-id={"7-0"}>{carryUnions(data.alt)}</div>
            </Waypoint>
              </div>
              <div className={`atom-col`}>
                <div className={`atom-text`} data-id={"7-0"}>{carryUnions(data.txt)}</div>
              </div>
            </div>

            :
            

            <Fragment>
              <div className={`atom-row`}>
                <div className={`atom-col`}>
                  <div className={`atom-alt`} data-id={"7-1"}>{data.alt}</div>
                </div>
                <div className={`atom-col`}>
                </div>
              </div>


              <div className={`atom-row`} data-id={"7-0"}>
                <div className={`atom-col`} data-id={"7-0"}>



              <Newimg 
                id={"7-0"} 
                tid={"7-0"} 
                src={`img/${storie}/${storie}_${data.imgl}.jpg`}
                srcset={`img/${storie}/${storie}_${data.imgl}_2x.jpg 2x`}
                alt={carryUnions(data.altl)}
                mobile={this.props.mobile} 
              />


                </div>

                <div className={`atom-col`} data-id={"7-1"}>
                  <div className={`atom-text`} data-id={"7-0"}>{data.txt}</div>
                </div>

              </div>
            </Fragment>
          }

        </section>
      </Waypoint>
    );
  }
}
