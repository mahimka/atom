import React from 'react';
import html from 'react-inner-html';
import { Waypoint } from 'react-waypoint';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'

// import Vidos from '../../@ips/react/components/vidos'
import { carryUnions } from '../../utils/typo.js'

export default class Start extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      startmenu: false
    }
    this.handleWaypointEnter = this.handleWaypointEnter.bind(this);
    this.setStartmenu = this.setStartmenu.bind(this);
  }

  componentDidMount() {
    gsap.set(`.atom-start-menu-texts-block`,{y:(-153.8 - 45)})
    gsap.to(`.atom-start-menu-texts-block`,{y:0, repeat: -1, duration: 6, ease: "none" })
  }


  setStartmenu(val){
    this.setState({startmenu: val})
  }

  handleWaypointEnter(props){

  }
  handleWaypointLeave(props){

  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevState.startmenu != this.state.startmenu){
      if(this.state.startmenu){

        let mi = Array.prototype.slice.call(document.querySelectorAll(`.atom-start-menu-button`)).reverse();

        gsap.killTweensOf(mi);
        gsap.set(mi,{opacity:0, x:200, pointerEvents: 'none'})
        gsap.to(mi,{opacity:1, x:0, duration: 0.5, stagger: 0.04, ease: "power3.inOut",onComplete: function(){
          gsap.set(mi,{pointerEvents: 'auto'})
        }})
      }
      else{

      }
    }
  }





  render() {
    const data = this.props.data;

    const str = [
    `Энергия сети`,
    `В РИТМе жизни`,
    `Чистое будущее`,
    `Огни города`,
    `Счастливого пути`,
    `Доброе утро`,
    `Из России с любовью`
    ]




    return (
      <Waypoint scrollableAncestor={window} bottomOffset={"0%"} fireOnRapidScroll={false}
        onEnter={(props) => {this.handleWaypointEnter(props)}}
        onLeave={(props) => {this.handleWaypointLeave(props)}}
      >
        <section className={`atom-section`} data-id={"start"}>
          

          <div className={`video-frame`}>
              
              <video 
                className="atom-video" 
                autoPlay
                muted
                loop
                poster={(this.props.mobile) ? `media/coverm.jpg` : `media/cover.jpg`}
                src={(this.props.mobile) ? `media/cover.mp4` : `media/coverm.mp4`}
              />
              <div className={`atom-video-header`}>
                <div>{`ЛЮДИ — ЛЮДЯМ`}</div>
                <div className={`atom-arrowm`} {...html(require(`!raw-loader!../../assets/arrowm.svg`))} />
              </div>

              <div className={`atom-start-menu`}>
                
                <div className={`atom-start-menu-btn`}
                  onMouseEnter={()=>( this.setStartmenu(true) )}
                  onMouseLeave={()=>( this.setStartmenu(false) )}
                >
                  <div className={`atom-start-menu-texts-block`}>
                    <div className={`atom-start-menu-texts`}>
                      <span className={`atom-start-menu-text`}>{`СЮЖЕТЫ`}</span>
                      <span className={`atom-start-menu-text`}>{`СЮЖЕТЫ`}</span>
                      <span className={`atom-start-menu-text`}>{`СЮЖЕТЫ`}</span>
                      <span className={`atom-start-menu-text`}>{`СЮЖЕТЫ`}</span>
                    </div>
                  </div>
                </div>

                <div className={`atom-start-menu-btns` + ((this.state.startmenu)?" atom-start-menu-btns-visible":"")}>
                {
                  str.map((d, i)=>(
                    <div 
                      className={`atom-start-menu-button`}
                      data-id={i}
                      key={i}
                      onClick={()=>{
                        this.props.setStart(false, i);
                      }}
                      onMouseEnter={()=>( this.setStartmenu(true) )}
                      onMouseLeave={()=>( this.setStartmenu(false) )}
                    >
                      <div className={`atom-start-menu-image`}>
                        <img src={`img/menu${i}_2x.jpg`} className={`atom-start-menu-image-img`} />
                      </div>

                      <div className={`atom-start-menu-button-txts`}>
                        <span className={`atom-start-menu-button-txt1`} >{`Сюжет ` + (i+1)}</span>
                        <span className={`atom-start-menu-button-txt2`} >{d}</span>
                      </div>
                    </div>
                  ))
                }
                </div>

              </div>

          </div>


          <div className={`atom-start-menu-m`}>
            {
              str.map((d, i)=>(
                <div 
                  className={`atom-start-menu-button-m`}
                  data-id={i}
                  key={i}
                  onClick={()=>{
                    this.props.setStart(false, i);
                  }}
                  onMouseEnter={()=>( console.log(1) )}
                  onMouseLeave={()=>( console.log(2) )}
                >
                  <div className={`atom-start-menu-button-txts-m`}>
                    <span className={`atom-start-menu-button-txt1-m`} >{`cюжет ` + (i+1)}</span>
                    <span className={`atom-start-menu-button-txt2-m`} >{d}</span>
                  </div>
                </div>
              ))
            }

          </div>


          <div className={`atom-start-bottom`}>
            <div className={`atom-start-bottom-text`}>
              {carryUnions(`Фотопроект о людях, работающих в машиностроении, результатах их труда и тех, чью жизнь они делают комфортнее и безопаснее — всех нас`)}
            </div>
          </div>

        </section>
      </Waypoint>
    );
  }
}
