import React from 'react';
import html from 'react-inner-html';

import { gsap } from "gsap/dist/gsap";
import { EasePack } from 'gsap/all'

import { connect, post, event } from '../../vendor/gtm.js'


export default class Stories extends React.Component {
  constructor(props) {
    super(props);

    this.state={}
  }

  componentDidMount() {

  }

  render() {

    const data = [
    `Энергия сети`,
    `В ритме жизни`,
    `Чистое будущее`,
    `Огни города`,
    `Счастливого пути`,
    `Доброе утро`,
    `Из России с любовью`
    ]


    return (
      <div className={`atom-stories` + ((this.props.visible)?` atom-stories-visible`:``)}>

        <div className={`atom-stories-header`}>{`ЛЮДИ — ЛЮДЯМ`}</div>
        <div className={`atom-stories-sheader`} dangerouslySetInnerHTML={{__html: `Проект МИА «Россия сегодня» при&nbsp;участии&nbsp;АО&nbsp;«Атомэнергомаш»` }} />

        {
          data.map((d, i)=>(
            <div 
              className={`atom-stories-button`}
              data-id={i}
              key={i}
              onClick={()=>{
               this.props.setStart(false, i);
              }}
            >
              <div className={`atom-stories-button-txt1`} >{`Сюжет ` + (i+1)}</div>
              <div className={`atom-stories-button-txt2`} >{d}</div>
            </div>
          ))
        }

        <div 
          className={`atom-stories-button-txt3`}
          onClick={()=>{
           this.props.setStart(true);
          }}
        >{"ГЛАВНАЯ СТРАНИЦА"}</div>


        <div 
          className={`atom-stories-cross`} 
          {...html(require(`!raw-loader!../../assets/cross2.svg`))}
          onClick={()=>{
            this.props.setStories(false);
          }}
        />

      </div>
    );
  }
}
