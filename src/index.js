// import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import dt from '../static/data/data.json';
import { connect, post, event } from './vendor/gtm.js'


function ready() {

	const root = document.getElementById('atom20');
	root.setAttribute('data-ua', navigator.userAgent);
	
	window.app = {}
	window.app.projectName = "atom"
	window.app.version = "d"

	connect(app)
	event('entrance')

	ReactDOM.render(<App data={dt} />, root);
}

ready()

// document.addEventListener("DOMContentLoaded", ready);
