
let _ = require('lodash');

let POST_EXCUSES = ["мы", "она", "они", "оно", "я", "что", "все", "без", "безо", "близ", "в", "во", "вместо", "вне", "для", "до", "за", "из", "изо", "из-за", "из-под", "к", "ко", "кроме", "между", "меж", "на", "над", "надо", "о", "об", "обо", "от", "ото", "перед", "передо", "пред", "предо", "по", "под", "подо", "при", "про", "ради", "с", "со", "у", "через", "чрез", "так", "и", "да", "не", "но", "тоже", "ни", "зато", "однако", "же", "либо", "то", "ли", "а", "я", "он"];

let PREV_EXCUSES = ["века", "в.", "год", "г.", "года", "—", "–", "-"];

export function carryUnions(text) {
    let words = text.replace(/ +(?= )/g,'').replace(/></g, "> <").split(" ");
    text = '';

    _.each(words, function(word, i) {
        word = word.replace(/[,;:!?()"]/g, '').replace(/<.*?>/gi, '').toLowerCase();
        var space = "";

        if (_.indexOf(POST_EXCUSES, word) != -1) {
            text += words[i];
            space = "\u00A0";
        } else if(_.indexOf(PREV_EXCUSES, word) != -1 && i) { // skip if it is a first word in the text
            text = text.substr(0, text.length - 1) + "\u00A0" + words[i];
            space = " ";
        } else {
            text += words[i];
            space = " ";
        }
        if(i < (words.length - 1)) {
            text += space;
        }
    });

    return text;
}