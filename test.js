

if(window.location.href=="https://www.contour.network/explore"){

	let timerId = setInterval(function(){

		var header = document.getElementsByTagName('header')[0];
		var headerHeight = window.getComputedStyle(header).height;

		var iframe = getIframe();
		if(iframe){
			iframe.setAttribute("style", `position:relative; width: 100vw; height:calc(100vh - ${headerHeight});`);
			iframe.focus();
	    	iframe.contentWindow.focus();
		}

		var vixiframe = getWixIframe();
		if(vixiframe){
	    	vixiframe.setAttribute("style", "width:100% !important; height:100% !important; margin: 0 !important; left: 0px !important;");
		}

		if(iframe && vixiframe){
			console.log("clear interval")
			clearInterval(timerId);
		}


		var main = document.getElementsByTagName("main")[0];
		main.setAttribute("style", "overflow-x:hidden !important;");

	}, 1000);
}


function setIframeFocus() {
	var header = document.getElementsByTagName('header')[0];
	var headerHeight = window.getComputedStyle(header).height;

	var iframe = getIframe();
	if(iframe){
		iframe.setAttribute("style", `position:relative; width: 100vw; height:calc(100vh - ${headerHeight});`);
		iframe.focus();
    	iframe.contentWindow.focus();
	}

	var vixiframe = getWixIframe();
	if(vixiframe){
    	vixiframe.setAttribute("style", "width:100% !important; height:100% !important; margin: 0 !important; left: 0px !important;");
	}

	var main = document.getElementsByTagName("main")[0];
	main.setAttribute("style", "overflow-x:hidden !important;");
}


function getWixIframe(){
	var iframes = document.getElementsByTagName("wix-iframe");
	for(var i=0; i<iframes.length; i++){
		console.log(iframes[i].getAttribute('id'))
		if(iframes[i].getAttribute('id') == 'comp-kg2hyd43'){
			return iframes[i]
		}
	}
}

function getIframe(){
	var iframes = document.getElementsByTagName('iframe');
	for(var i=0; i<iframes.length; i++){
		console.log(iframes[i].getAttribute('id'))
		if(iframes[i].src == 'https://www.alphagridinfographics.com/contour/'){
			return iframes[i]
		}
	}
}