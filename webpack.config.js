const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = (env, options) => {

  const PROD = options.mode === 'production';
  const DEV = options.mode === 'development';

  return {

  entry: { main: './src/index.js' },
  
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[hash].js'
  },

  resolve: {
    modules: [
      path.resolve(__dirname, 'node_modules'),
      path.resolve(__dirname, 'src'),
    ]
  },  
  
  optimization: {
    minimize: PROD,
    minimizer: [new TerserPlugin({
        test: /\.js(\?.*)?$/i,
      })],
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/i,
        use:  [
          'style-loader', 
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: true,
              reloadAll: true,
            },
          },
          'css-loader', 
          'postcss-loader'
        ]
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'url-loader',
        options: {
          outputPath: 'img',
          // limit: 300000,
          limit: 100000,
          name: '[name].[ext]',
        },
      },

      {
        test: /\.svg$/,
        use: [
          {
            loader: '@svgr/webpack',
            options: {
              svgo: false,
              svgoConfig: {
                "plugins": [{ "prefixIds": {"prefixIds": false, "prefixClassNames": false} }]
              },
            },
          },
        ],
      },


// https://react-svgr.com/docs/options/
// https://gist.github.com/pladaria/69321af86ce165c2c1fc1c718b098dd0


      {
        test: /\.(eot|ttf|woff|woff2)$/i,
        loader: 'url-loader',
        options: {
          outputPath: 'fonts',
          limit: 1000,
        },
      },

    ]
  },
  
  plugins: [ 

    new MiniCssExtractPlugin({
      filename: !PROD ? '[name].css' : '[name].[hash].css',
      chunkFilename: !PROD ? '[id].css' : '[id].[hash].css',
    }),
    new HtmlWebpackPlugin({
      title: 'atom20',
      hash: true,
      inject: false,
      template: './src/index.html',
      filename: 'index.html'
    }),
    new WebpackMd5Hash(),
    new CopyWebpackPlugin([path.resolve(__dirname, 'static')]),

    // PROD-only plugins
    ...(PROD ? [
          new CleanWebpackPlugin(),
          new BundleAnalyzerPlugin({ analyzerMode: 'static' }),
        ] : []),
  ],

  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    watchContentBase: false,
    compress: true,
    host: '0.0.0.0',
    hot: true,
    disableHostCheck: true
  }
}

};